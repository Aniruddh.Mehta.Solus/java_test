package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;



@Entity
@Immutable
public class CustFirstName {
	@Id
	@Column(name="Name")
	private String Name;
	public  String getName(){ return  Name ;}

}
