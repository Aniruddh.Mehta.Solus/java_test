package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.Solus_Microservice.reco_API.entity.RcoreStory;

@Repository
public interface RcoreStoryRepo  extends JpaRepository<RcoreStory,String>{
	@Query(value="select a.Story_Id,a.Name,b.recos,c.Customer_Key from RankedPickList_Stories_Master a, RankedPickList_Stories b, CDM_Customer_Key_Lookup c where a.Story_Id=b.Story_Id and c.Customer_Key=:cust_key and a.Name=:name and c.Customer_Id=b.Customer_Id  ", nativeQuery=true)
	public List<RcoreStory> getStoryDetails(@Param("name") String storyName,@Param("cust_key") String customer_key); 
}
