package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;


//Microsite_URL, Full_Name, Email, Mobile, PN_Message
@Entity
@Immutable
public class LandingPageRecos {
	@Id
	@Column(name="Microsite_URL")
	private String microsite_url;
	public  String getmicrosite_url(){ return microsite_url ;}
	
	@Column(name="Full_Name")
	private String full_name;
	public  String getfull_name(){ return full_name ;}
	
	@Column(name="Email")
	private String email;
	public  String getemail(){ return email ;}
	
	@Column(name="Mobile")
	private String mobile;
	public  String getmobile(){ return mobile ;}
	
	@Column(name="PN_Message")
	private String pn_message;
	public  String getpn_message(){ return pn_message ;}

}
