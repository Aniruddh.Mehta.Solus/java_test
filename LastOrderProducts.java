package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
public class LastOrderProducts {
	@Id
	@Column(name="Product_Key")
	private String productKey;
	public  String getproductId(){ return productKey ;}

}
