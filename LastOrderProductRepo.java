package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.Solus_Microservice.reco_API.entity.LastOrderProducts;
@Repository
public interface LastOrderProductRepo extends JpaRepository<LastOrderProducts,String>{
	@Query(value=" select c.Product_Key from CDM_Customer_Key_Lookup a, CDM_Bill_Details b ,CDM_Product_Key_Lookup c where a.Customer_Id=b.Customer_Id and b.Product_Id=c.Product_Id and a.Customer_Key=:cust_key   and b.Bill_Header_Id in (select max(Bill_Header_Id) from  CDM_Customer_Key_Lookup a, CDM_Bill_Header b where a.Customer_Id=b.Customer_Id and a.Customer_Key=:cust_key ) ", nativeQuery=true)
	public List<LastOrderProducts> getLastOrderProductList(@Param("cust_key") String Customer_Key);
}
