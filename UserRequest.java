package com.Solus_Microservice.reco_API.entity;

import java.util.List;

import org.hibernate.cache.spi.support.AbstractReadWriteAccess.Item;

public class UserRequest {
	private String user_id;
	private String created_at;
	private int sum;
	private String restuarant_id; 
	private int slot_num;
	private String device;
	private String session_id;
	private String order_type;
	private String upscale_src;
	private List<Item> items;
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public int getSum() {
		return sum;
	}
	public void setSum(int sum) {
		this.sum = sum;
	}
	public String getRestuarant_id() {
		return restuarant_id;
	}
	public void setRestuarant_id(String restuarant_id) {
		this.restuarant_id = restuarant_id;
	}
	public int getSlot_num() {
		return slot_num;
	}
	public void setSlot_num(int slot_num) {
		this.slot_num = slot_num;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getSession_id() {
		return session_id;
	}
	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}
	public String getOrder_type() {
		return order_type;
	}
	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}
	public String getUpscale_src() {
		return upscale_src;
	}
	public void setUpscale_src(String upscale_src) {
		this.upscale_src = upscale_src;
	}
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", created_at=" + created_at + ", sum=" + sum + ", restuarant_id="
				+ restuarant_id + ", slot_num=" + slot_num + ", device=" + device + ", session_id=" + session_id
				+ ", order_type=" + order_type + ", upscale_src=" + upscale_src + ", items=" + items + "]";
	}
}
