package com.solus.solus_url_shotener_service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
public class EventMaster {
	@Id
	@Column(name="ID")
	private long id;                                                  
	@Column(name="ChannelName")
	private String channel;                                            
	@Column(name="Website_URL")
	private String websiteurl;                                          
	@Column(name="Name")
	private String name;
	@Column(name="Description")
	private String description;
	public   long getid(){ return  id ;}                                                                                      
	public   String getchannel(){ return  channel ;}                                         
	public   String getwebsiteurl(){ return  websiteurl ;}                             
	public  String getname(){ return  name ;}
	public  String getdescription(){ return  description ;}
}
