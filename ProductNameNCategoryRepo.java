package com.solus.solus_url_shotener_service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.solus.solus_url_shotener_service.entity.ProductNameNCategory;

public interface ProductNameNCategoryRepo  extends JpaRepository<ProductNameNCategory,String>{
	@Query(value=" select a.Product_Name as Product_Name, l4.LOV_Value  , a.Custom_Tag1_LOV_Id as Bought, a.Custom_Tag2_LOV_Id as Watchlist,  case a.Custom_Tag3_LOV_Id when 0 then 'NA' else l1.LOV_Value end as CMOTS_COCODE,  case a.Custom_Tag4_LOV_Id when 0 then 'NA' else l2.LOV_Value end as ISIN,  case a.Custom_Tag5_LOV_Id when 0 then 'NA' else l3.LOV_Value end as EXCHANGEE,  case a.Custom_Tag6_LOV_Id when 0 then 'NA' else substring_index(l5.LOV_Value,':',-1) end as ODINCODE   from CDM_Product_Master a  left join CDM_Product_Key_Lookup b on a.Product_Id=b.Product_Id  left join CDM_LOV_Master l1 on l1.LOV_Id=a.Custom_Tag3_LOV_Id  left join CDM_LOV_Master l2 on l2.LOV_Id=a.Custom_Tag4_LOV_Id  left join CDM_LOV_Master l3 on l3.LOV_Id=a.Custom_Tag5_LOV_Id  left join CDM_LOV_Master l4 on l4.LOV_Id=a.Cat_LOV_Id   left join CDM_LOV_Master l5 on l5.LOV_Id=a.Custom_Tag6_LOV_Id   where b.Product_Key=:prod_key ", nativeQuery=true)
	public List<ProductNameNCategory>getProductNameNCategoryList(@Param("prod_key") String product_key);
}


//@Query(value="select a.Product_Name,b.LOV_Value  from CDM_Product_Master a,CDM_LOV_Master b,CDM_Product_Key_Lookup c where c.Product_Id=a.Product_Id and b.LOV_Id=a.Cat_LOV_Id and c.Product_Key=:prod_key", nativeQuery=true)
//select a.Product_Name, b.LOV_Value , a.Custom_Tag1_LOV_Id as Bought,  a.Custom_Tag2_LOV_Id as Watchlist, case a.Custom_Tag3_LOV_Id when -1 then 'NA' else l1.LOV_Value end as CMOTS_COCODE, case a.Custom_Tag4_LOV_Id when -1 then 'NA' else l2.LOV_Value end as ISIN,  case a.Custom_Tag5_LOV_Id when -1 then 'NA' else l3.LOV_Value end as EXCHANGEE  from CDM_Product_Master a,CDM_LOV_Master b,CDM_Product_Key_Lookup c  left join CDM_LOV_Master l1 ON l1.LOV_Id=b.Custom_Tag3_LOV_Id  left join CDM_LOV_Master l2 ON l2.LOV_Id=b.Custom_Tag4_LOV_Id  left join CDM_LOV_Master l3 ON l3.LOV_Id=b.Custom_Tag5_LOV_Id where c.Product_Id=a.Product_Id and b.LOV_Id=a.Cat_LOV_Id and c.Product_Key=:prod_key 