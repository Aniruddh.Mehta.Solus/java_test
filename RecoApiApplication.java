package com.Solus_Microservice.reco_API;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.Solus_Microservice.reco_API.service.FileReaderUtility;

//@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
@SpringBootApplication
@EnableAsync
//@ComponentScan("com.Solus_Microservice")
public class RecoApiApplication extends SpringBootServletInitializer {
	public static String logLevel = "";
	public static String logFilePath = "";
	public static void main(String[] args) {
		logFilePath = args[1];
		System.out.println("Starting Solus Microservice V2.1 30-Dec-22");
		try {
			logLevel = args[2];
			if(logLevel.equals("TEST")) {
				System.out.println("Log Level == Test ");
			}
			if(!(logLevel.equals("TEST")||logLevel.equals("PROD"))) {
				System.out.println("Logging level is not defined. (TEST/PROD)");
				System.exit(0);
			}
		}catch(Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("Loading Data in memory from Flat files...");
			if(logLevel.equals("TEST"))
				System.out.println("Log Level == Test ");
			// # List of files for the end 
			Instant start_time = Instant.now();
			String rankedPickList_File = args[0];
			String genome_File = args[3];
			String ibcf_File = args[4];
			String preference_File = args[4];
			if(rankedPickList_File.contains("--")) {
				System.out.println("rankedPickList file path, Trending file path and IBCF file path arguments are missing");
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " rankedPickList file path, Trending file path and IBCF file path arguments are missing");
				System.exit(0);
			}
			FileReaderUtility file = new FileReaderUtility();
			file.loadData(rankedPickList_File,logLevel,genome_File,ibcf_File,preference_File);
			Instant time1 = Instant.now();
			System.out.println("Completed Loading Data");
			System.out.println("Load time: " + ChronoUnit.MILLIS.between(start_time, time1) + " ms");
			if(logLevel.equals("TEST")) {
				System.out.println("Completed Loading Data");
			}
			
			}catch(Exception e) {
				System.out.println("Error while loading data - "+e);
				System.out.println(Instant.now() + " | ERROR | " + e);
				System.exit(0);
			}
		try {
			SpringApplication.run(RecoApiApplication.class, args);
		}catch(Exception e) {
			System.out.println("Error while starting API - "+e);
			System.exit(0);
		}
	}
	// For creating war file
		@Override
		protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
			return application.sources(RecoApiApplication.class);
		}
		
		@Bean
		public PasswordEncoder passwordEncoder() {
			return NoOpPasswordEncoder.getInstance();
		}
}
