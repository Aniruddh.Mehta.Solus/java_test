package com.solus.solus_url_shotener_service.entity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Microservice_Config")
@Access(value=AccessType.FIELD)
public class RouteState {
	@Id
	@Column(name="ID")
	private long id;                                                  
	@Column(name="Name")
	private String name;                                            
	@Column(name="Value")
	private String value;                                          
	@Column(name="Description")
	private String description;                              
	@Column(name="Modified_Date")
	private String modifiedDate;                           
	public   long getiD(){ return  id ;}                                                 
	public   String getname(){ return  name ;}                                           
	public   String getvalue(){ return  value ;}                                         
	public   String getdescription(){ return  description ;}                             
	public  String getmodifiedDate(){ return  modifiedDate ;}                            
	public  void setiD( long iD ){ this.id=id;}                                          
	public  void setname( String name ){ this.name=name;}                                
	public  void setvalue( String value ){ this.value=value;}                            
	public  void setdescription( String description ){ this.description=description;}    
	public  void setmodifiedDate(String modifiedDate ){ this.modifiedDate=modifiedDate;}

}
