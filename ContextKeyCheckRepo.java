package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.Solus_Microservice.reco_API.entity.ContextKeyCheck;

@Repository
public interface ContextKeyCheckRepo  extends JpaRepository<ContextKeyCheck,String>{
	@Query(value=" select Story_Id from RankedPickList_Anonymous_Stories_Master where Object_Type=:contKey and Type like '%trend%' ", nativeQuery=true)
	public List<ContextKeyCheck> getContectKeyList(@Param("contKey") String Context_Key);
}
