package com.solus.solus_url_shotener_service.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

@Component
public class HttpLoggerStatic extends AbstractRequestLoggingFilter{
	
	private String remoteHost;
	private int remotePort;
	private Instant currentTimestamp;
	private String logFilePath = "URL_Shortner_Service_Logs/";
	private String filename = "STATIC_Request_Logs_" 
			+ (new SimpleDateFormat("yyyy_MM_dd").format(new Date()))
			+ ".txt";

	public void logWriter(String content) {
		try {
		    final Path path = Paths.get(logFilePath,filename);
		    Path pathParent = path.getParent();
		    if(!Files.exists(pathParent)) {
		    	Files.createDirectories(pathParent);
		    }
		    Files.write(path, Arrays.asList(content), StandardCharsets.UTF_8,
		        Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		} catch (final IOException ioe) {
			    // Add your own exception handling...
				System.out.println(ioe);
				}
		}

	@Override
	protected void beforeRequest(HttpServletRequest request, String message) {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void afterRequest(HttpServletRequest request, String message) {
		// TODO Auto-generated method stub
		
	}
	

}
