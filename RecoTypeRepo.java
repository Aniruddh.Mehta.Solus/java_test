package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.Solus_Microservice.reco_API.entity.RecoTypeCheck;

@Repository
public interface RecoTypeRepo  extends JpaRepository<RecoTypeCheck,String>{
	@Query(value="select Story_Id,UseCaseKey from RankedPickList_Stories_Master where UseCaseKey=:usecasekey ", nativeQuery=true)
	public List<RecoTypeCheck> getRecoTypeDetails(@Param("usecasekey") String reco_type);
}