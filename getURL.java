package com.solus.solus_url_shotener_service.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.solus.solus_url_shotener_service.SolusUrlShotenerServiceApplication;
import com.solus.solus_url_shotener_service.service.ApiService;

@RestController
public class getURL  extends HttpServlet{
	@Autowired
	private ApiService service;
	
	@Autowired
	private FileReaderUtility file;
	/*
	 * This modification is in tune to runnning the shortener in a standalone location 
	 * where Multi-Tenanat Redirect is not working 
	 * Changes made to this version 
	 * 1) Removed First redirect from the service 
	 * 2) Make the output format of the functions as same 
	 * 3) Increase the index of the Microsite URL Reading
	 */
	
	HttpLogger logger=new HttpLogger();
	HttpLoggerStatic staticlogger=new HttpLoggerStatic();
	SolusUrlShotenerServiceApplication s=new SolusUrlShotenerServiceApplication();

	@GetMapping("/{urlParameter}")
	public String restServiceWithUTMRedirect(@PathVariable("urlParameter") String urlParameter, HttpServletResponse httpResponse,HttpServletRequest request)
			throws Exception {
		/*
		 * Access the URL Config file 
		 * check the central server 
		 * redirect to the desired server second funtion 
		 * return null 
		 * log each step 
		 * 
		 * 
		 */
		try (InputStream input = new FileInputStream(s.config_filename)) {
			Properties prop = new Properties();
			prop.load(input);
			String response="NA";
			System.out.println("Config file recieved !");
			System.out.println("Get Local Command:: "+ request.getLocalName());
			System.out.println(prop.getProperty("central_server"));
			if(request.getLocalName().equals(prop.getProperty("central_server")))
			{
				httpResponse.reset();
				response=prop.getProperty(urlParameter.substring(0, 1))+"/ss/"+urlParameter.substring(1);
				System.out.println(response);
				httpResponse.sendRedirect(response);
				return null;
			}
		}catch(IOException ex)
		{
			System.out.println("Error:" + ex);
		}
		return null;
	}
	
	
	@GetMapping("/ss/{urlParameter}")
	public String restServiceWithUTM(@PathVariable("urlParameter") String urlParameter, HttpServletResponse httpResponse)
			throws Exception {
		/*
		 * Connect to DB
		 * Check the Flag
		 * Check presence of others 3 files 
		 * Add health flags 
		 * Add separate logging for each case- Regular,static and error 
		 * Recompile in Java 8  
		 * event_mapping_filename=args[1];
		 * url_code_mapping_filename=args[2];
		 * static_url_file=args[3];
		 */
		
		String event_id = "NA";
		String encoded_event_id = "NA";
		String response = "NA";
		String utm_params = "NA";
		String channel="NA";
		String trigger_name="NA";
		String web_url="NA";
		String web_description="NA";
		String stateName ="shortener_url_route";
		
		try {
// ########################################################################################################
// ##################### redirect in case of Static URL ###################################################
			InputStream input = new FileInputStream(s.config_filename);
			Properties prop = new Properties();
			prop.load(input);
			response=prop.getProperty("static_page_url/"+urlParameter.substring(1,3));
			System.out.println("property static URL: "+prop.getProperty("static_page_url/"+urlParameter.substring(0,2)));
			//System.out.println("response.equals(\"NA\") ="+response.equals("NA"));
			//equals(null)
			if (!(response==null)) {
				staticlogger.logWriter(Instant.now()+"|"+response);
				System.out.println("Static URL Accessed"+ response);
				httpResponse.sendRedirect(response);
				return null;
			}
// ##########################################################################################################
			Map<String, String> response_l = new HashMap<>();
			Map<String, String> response_j = new HashMap<>();
			String response_band_2 = "";
			String response_band_3 =  "";
			String response_band_4 =  "";
			// Add stories for bands
			response_band_2=service.getRecosBand_2();
			response_l = service.getMicroserviceState(stateName);
			String stateOutput="";
			stateOutput=response_l.get("State");
			String bitly_code="";
			String recolist_csv="";
			String recolist_csv_recospids="";
			String customerkey="";
			String CustomerFirstName="";
			if (!(stateOutput.equals("test"))) {
			bitly_code=urlParameter;
			response_j = service.getRecoList(bitly_code);
			recolist_csv=response_j.get("PN_Message");
			recolist_csv_recospids=service.getRecosBand_1(recolist_csv);
			customerkey=response_j.get("Customer_Key");
			CustomerFirstName=service.getFirstName(customerkey);
			response_band_3=service.getRecosBand_3(customerkey);
			response_band_4=service.getRecosBand_4();
			}
// ########################################################################################################
// ###################### Default Path ####################################################################
			if (stateOutput.equals("default")) {
				System.out.println("State of db_route = "+stateOutput);
				encoded_event_id=urlParameter.substring(0,2);
				System.out.println("encoded_event_id: "+ encoded_event_id);
				event_id=Integer.toString(file.decode(encoded_event_id));
				System.out.println("Event Id"+ event_id);
				// Event Data Structure
				Map<String, String> event_details = new HashMap<String, String>();
				if(event_id.equals(""))
				{
					return null;
				}
				event_details=file.getEventData(event_id);
				channel=event_details.get("Channel");
				trigger_name=event_details.get("Trigger_Name");
				web_url=event_details.get("Web_URL");
				System.out.println("|channel="+channel+"|trigger_name="+trigger_name+"|web_url="+web_url);
				
				if (channel.equals("SMS"))
				{
					channel="SMS_solus";
				}
				if (channel.equals("EMAIL"))
				{
					channel="EMAIL_solus";
				}
				if (channel.equals("PN"))
				{
					channel="PN_solus";
				}
				System.out.println("#########################recolist_csv_recospids#####################################");
				System.out.println(recolist_csv_recospids);
				utm_params="&utm_source=ss"+"&utm_medium="+channel+"&utm_name="+CustomerFirstName+"&utm_msg="+web_description+"&utm_campaign="+trigger_name+"&utm_band_1=";
				//response= web_url+urlParameter+utm_params+recolist_csv;
				response= web_url+urlParameter+utm_params+recolist_csv_recospids;
				response=response+"&utm_band_2="+response_band_2+"&utm_band_3="+response_band_3+"&utm_band_4="+response_band_4;
				System.out.println(response);
				logger.logWriter(Instant.now()+"|"+channel+"|"+trigger_name+"|"+response);
				httpResponse.sendRedirect(response);
				return null;
			
			}
// ###############################################################################################################
// ######################################### DB Access ###########################################################
			else if(stateOutput.equals("db_access")) {
				System.out.println("State of db_route = "+stateOutput);
				encoded_event_id=urlParameter.substring(0,2);
				event_id=Integer.toString(file.decode(encoded_event_id));
				System.out.println("Event Id"+ event_id);
				// Event Data Structure
				if(event_id.equals(""))
				{
					return null;
				}
				Map<String, String> event_map = new HashMap<String, String>();
				event_map=service.getEventMasterDetails(file.decode(encoded_event_id));  //
//				event_id=Integer.toString(event_map.get("ChannelName"));
//				event_details=file.getEventData(event_id);
//				channel=event_details.get("Channel");
//				trigger_name=event_details.get("Trigger_Name");
//				web_url=event_details.get("Web_URL");
				
				channel=event_map.get("ChannelName");
				trigger_name=event_map.get("Name");
				web_url=event_map.get("Website_URL");
				web_description=event_map.get("Description");
				System.out.println("|channel="+event_map.get("ChannelName")+"|trigger_name="+event_map.get("Name")+"|web_url="+event_map.get("Website_URL"));
				
				if (channel.equals("SMS"))
				{
					channel="SMS_solus";
				}
				if (channel.equals("EMAIL"))
				{
					channel="EMAIL_solus";
				}
				if (channel.equals("PN"))
				{
					channel="PN_solus";
				}
				System.out.println("#########################recolist_csv_recospids#####################################");
				System.out.println(recolist_csv_recospids);
				utm_params="&utm_source=ss"+"&utm_medium="+channel+"&utm_campaign="+trigger_name+"&utm_name="+CustomerFirstName+"&utm_msg="+web_description+"&utm_key="+customerkey+"&utm_band_1=";
				response= web_url+urlParameter+utm_params+recolist_csv_recospids;
				response=response+"&utm_band_2="+response_band_2+"&utm_band_3="+response_band_3+"&utm_band_4="+response_band_4;
				System.out.println(response);
				logger.logWriter(Instant.now()+"|"+channel+"|"+trigger_name+"|"+response);
				httpResponse.sendRedirect(response);
				return null;
				}
// ###############################################################################################################
// ######################################### Test Data ###########################################################
			else if(stateOutput.equals("test")) {
				String test_status="";
				System.out.println("URL Parameter="+urlParameter);
				test_status=file.getTestDataStatus(urlParameter);
				return test_status; 
			}
			}catch (Exception e) {
				System.out.println("Error:" + e);
				}
		return event_id;
		}
}
