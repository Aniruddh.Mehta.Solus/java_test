package com.Solus_Microservice.reco_API.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService  implements UserDetailsService {
	
	@Value("${auth.client.username}")
	private String username;
	@Value("${auth.client.password}")
	private String password;
	@Value("${auth.client.id}")
	private String clientid;
	@Value("${auth.client.secret}")
	private String secret;
	@Value("${auth.granttype}")
	private String grant_type;
	@Value("${auth.client.scope}")
	private String scope;
	
	
    @Override
    public UserDetails loadUserByUsername(String client_id) throws UsernameNotFoundException {

        //Logic to get the user form the Database
    	if(!client_id.equals(clientid))
    		return null;
        return new User(clientid,secret,new ArrayList<>());
    }
	

}
