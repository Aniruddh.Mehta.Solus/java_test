package com.Solus_Microservice.reco_API.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;


@Component
public class FileReaderUtility {
	public static HashMap<String, String[]> rankedPickList = new HashMap<String, String[]>();
	public static HashMap<String, String[]> genomeRecoList = new HashMap<String, String[]>();
	public static HashMap<String, String[]> preferenceRecoList = new HashMap<String, String[]>();
	public static HashMap<String, String[]> ibcfRecoList = new HashMap<String, String[]>();
	public static HashMap<String, String[]> shopifyProductMaster = new HashMap<String, String[]>();
	public static List<String> sql_data_structure = new ArrayList<String>();
	public static Map<String, Object> sql_data_structure_l = new HashMap();
	public static Map<String, Object> rpl_stories = new HashMap();
	public static Map<String, Object> product_key_Name_map = new HashMap();
	public static Map<String, Object> landing_page_data = new HashMap();
	
	
	
	public void loadData(String hybridizer_file,String loglevel,String genome_file,String ibcf_file,String preference_file) {
		String url="";
		String user="";
		String password="";
		url=System.getProperty("spring.datasource.url");
		user=System.getProperty("spring.datasource.username");
		password=System.getProperty("spring.datasource.password");
		try {
			//##########################################################################################
			// Reading the Hybridizer File
			int index = 0;
			String customer_key = "";
			String line;
			String[] products = new String[600];
			
			//Reading RankedPickList File
			System.out.println("Reading data from: \n"+hybridizer_file);
			BufferedReader buf = new BufferedReader(new FileReader(hybridizer_file));
			while ((line = buf.readLine()) != null) {
				String previous_customer_key = customer_key;
				customer_key = line.substring(0,line.indexOf("|"));
				String product_key = line.substring((line.indexOf("|")+1),(line.indexOf("|", line.indexOf("|")+1)));
				if(customer_key.equalsIgnoreCase("Customer_Key")) {
					continue;
				}
				else if(!customer_key.equals(previous_customer_key)) {
					if(!previous_customer_key.equalsIgnoreCase("Customer_Key"))
						rankedPickList.put(previous_customer_key, products);
					index = 0;
					products = new String[products.length];
					products[index]=product_key;
					rankedPickList.put(customer_key, products);
				}
				else{
					index++;
					products[index]=product_key;
				}
			}
			buf.close();
			System.out.println("RankedPickList - Number of customers: "+rankedPickList.size());
			//##########################################################################################
			// Read Genome File
			index = 0;
			customer_key = "";
			products = new String[500];
			System.out.println("Reading data from: \n"+genome_file);
			BufferedReader buf1 = new BufferedReader(new FileReader(genome_file));
			while ((line = buf1.readLine()) != null) {
				String previous_customer_key = customer_key;
				customer_key = line.substring(0,line.indexOf("|"));
				String product_key = line.substring((line.indexOf("|")+1),(line.indexOf("|", line.indexOf("|")+1)));
				if(customer_key.equalsIgnoreCase("Customer_Key")) {
					continue;
				}
				else if(!customer_key.equals(previous_customer_key)) {
					if(!previous_customer_key.equalsIgnoreCase("Customer_Key"))
						genomeRecoList.put(previous_customer_key, products);
					index = 0;
					
					products = new String[products.length];
					products[index]=product_key;
					genomeRecoList.put(customer_key, products);
				}
				else{
					index++;
					products[index]=product_key;
				}
			}
			buf1.close();
			System.out.println("Genome - Number of customers: "+genomeRecoList.size());
			//##########################################################################################
			// Read IBCF File
			index = 0;
			String base_product_key = "";
			products = new String[300];
			System.out.println("Reading data from: \n"+ibcf_file);
			BufferedReader buf4 = new BufferedReader(new FileReader(ibcf_file));
			while ((line = buf4.readLine()) != null) {
				String previous_base_product_key = base_product_key;
				base_product_key = line.substring(0,line.indexOf("|"));
				String reco_product_key = line.substring((line.indexOf("|")+1),(line.indexOf("|", line.indexOf("|")+1)));
				if(base_product_key.equalsIgnoreCase("Base_Product_Key")) {
					continue;
				}
				else if(!base_product_key.equals(previous_base_product_key)) {
					if(!previous_base_product_key.equalsIgnoreCase("Base_Product_Key"))
						ibcfRecoList.put(previous_base_product_key, products);
					index = 0;
					products = new String[products.length];
					products[index]=reco_product_key;
					ibcfRecoList.put(base_product_key, products);
				}
				else{
					index++;
					products[index]=reco_product_key;
				}
			}
			buf4.close();
			System.out.println("IBCF - Number of Products: "+ibcfRecoList.size());
			
			//##########################################################################################
			// Read Preference File
			index = 0;
			customer_key = "";
			products = new String[300];
			System.out.println("Reading data from: \n"+preference_file);
			BufferedReader buf2 = new BufferedReader(new FileReader(preference_file));
			while ((line = buf2.readLine()) != null) {
				String previous_customer_key = customer_key;
				customer_key = line.substring(0,line.indexOf("|"));
				String product_key = line.substring((line.indexOf("|")+1),(line.indexOf("|", line.indexOf("|")+1)));
				if(customer_key.equalsIgnoreCase("Customer_Key")) {
					continue;
				}
				else if(!customer_key.equals(previous_customer_key)) {
					if(!previous_customer_key.equalsIgnoreCase("Customer_Key"))
						preferenceRecoList.put(previous_customer_key, products);
					index = 0;
					products = new String[products.length];
					products[index]=product_key;
					preferenceRecoList.put(customer_key, products);
				}
				else{
					index++;
					products[index]=product_key;
				}
			}
			buf2.close();
			System.out.println("Preference - Number of customers: "+preferenceRecoList.size());
			
			
			
		}catch(Exception e) {
			System.out.println(e);
			System.out.println(Instant.now() + " | ERROR | " + e);
			System.exit(0);
		}
// //////////////////////////////////////////////
		String story_key=System.getProperty("story_key");
		String query_1 = "select Customer_Id,Customer_State from CDM_Customer_Master;";
		String query_2 = "select Customer_Id,Recos from RankedPickList_Stories a, RankedPickList_Stories_Master b where a.Story_Id=b.Story_Id and b.UseCaseKey='"+story_key+"';";
		String query_3 = "select Product_Key,Product_Name from CDM_Product_Key_Lookup a , CDM_Product_Master_Original b where a.Product_id=b.Product_Id ;";
		String query_4 = "select eeh.Microsite_URL,CONCAT_WS('|',cpi.Full_Name,cpi.Email,cpi.Mobile,PN_Message ) as recos_agg  from Event_Execution_History eeh, CDM_Customer_PII_Master cpi where	cpi.Customer_Id=eeh.Customer_Id and eeh.Microsite_URL<>-1;";
		/*
		 * 	select eeh.Microsite_URL,CONCAT_WS("|",cpi.Full_Name,cpi.Email,cpi.Mobile,PN_Message ) as recos_agg  from Event_Execution_History eeh, CDM_Customer_PII_Master cpi where	cpi.Customer_Id=eeh.Customer_Id and eeh.Microsite_URL<>-1;
		 */
		
		try {
			System.out.println("url="+url);
			System.out.println("user="+user);
			System.out.println("password="+password);
			System.out.println("Query="+query_1);
			Connection conn = DriverManager.getConnection(url, user, password);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query_1);
            
            while (rs.next()) {
                String columnValue1 = rs.getString("Customer_Id");
                String columnValue2 = rs.getString("Customer_State");
                String concatenatedValues = columnValue1 + "|" + columnValue2;
                sql_data_structure.add(concatenatedValues);
                sql_data_structure_l.put(columnValue2, columnValue1);
            }

            // Print out the ArrayList
            //System.out.println(sql_data_structure);

            // Close the database connection
            rs.close();
            stmt.close();
            conn.close();
		} catch (SQLException ex) {
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
        }
		// 
		//########################################## Get Recos from Ranked_Pick_list_Stories
		//
		try {
			System.out.println("url="+url);
			System.out.println("user="+user);
			System.out.println("password="+password);
			System.out.println("Query="+query_2);
			Connection conn = DriverManager.getConnection(url, user, password);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query_2);
            
            while (rs.next()) {
                String columnValue1 = rs.getString("Customer_Id");
                String columnValue2 = rs.getString("Recos");
                rpl_stories.put(columnValue1, columnValue2);
            }

            // Close the database connection
            rs.close();
            stmt.close();
            conn.close();
		} catch (SQLException ex) {
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
        }
		// 
		//########################################## Get Recos from Ranked_Pick_list_Stories
		//
		try {
			System.out.println("url="+url);
			System.out.println("user="+user);
			System.out.println("password="+password);
			System.out.println("Query="+query_3);
			Connection conn = DriverManager.getConnection(url, user, password);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query_3);
            
            while (rs.next()) {
                String columnValue1 = rs.getString("Product_Key");
                String columnValue2 = rs.getString("Product_Name");
                product_key_Name_map.put(columnValue1, columnValue2);
            }


            // Close the database connection
            rs.close();
            stmt.close();
            conn.close();
		} catch (SQLException ex) {
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
        }
// //////////////////////////////////////////////
//		try {
//			System.out.println("url="+url);
//			System.out.println("user="+user);
//			System.out.println("password="+password);
//			System.out.println("Query="+query_4);
//			Connection conn = DriverManager.getConnection(url, user, password);
//            Statement stmt = conn.createStatement();
//            ResultSet rs = stmt.executeQuery(query_4);
//            
//            while (rs.next()) {
//                String columnValue1 = rs.getString("Microsite_URL");
//                String columnValue2 = rs.getString("recos_agg");
//                //System.out.println("columnValue1"+columnValue1+"-------columnValue2"+columnValue2);
//                
//                //landing_page_data.put(columnValue1, columnValue2);
//            }
//
//            // Close the database connection
//            rs.close();
//            stmt.close();
//            conn.close();
//		} catch (SQLException ex) {
//        System.out.println("SQLException: " + ex.getMessage());
//        System.out.println("SQLState: " + ex.getSQLState());
//        System.out.println("VendorError: " + ex.getErrorCode());
//        }

	}
	
//	public void loadRPLData(String rankedPickList_file,String rankedPickList_anon_file) {
//		try {
//			System.out.println("RPL File_Found: "+ rankedPickList_file);
//			System.out.println("RPL Anon File_Found: "+ rankedPickList_anon_file);
//		}catch(Exception e) {
//			System.out.println(e);
//			System.out.println(Instant.now() + " | ERROR | " + e);
//			System.exit(0);
//		}
//	}
	// ##################################################################################
	// Get Product master Details 
	public Map<String, String> getProductData(String product_key){
		Map<String, String> product_details = new HashMap<String, String>();
		product_details.put("Product_Variant_Id", product_key);
		return product_details;
	}
	
	// ###################################################################################
	// Get Product Reco Hybridizer 
	
	public List<String> getProductRecosHybridizer(String customer_key, int slot_num) {
		List<String> product_recos = new ArrayList<String>();
		//Customer_key is null or customer_key not present in RankedPickList
		if(customer_key.equals("-100") || rankedPickList.get(customer_key) == null) {
			if(rankedPickList.get(customer_key) == null){
				System.out.println(customer_key + " is not present in DB");
				return product_recos;
			}	
		}
		// When customer Present in the system 
		else {
			int no_of_rankedPickList = slot_num;
			int i=0;
			int count_recos=0;
			while(rankedPickList.get(customer_key)[i]!=null) {
				if(count_recos==no_of_rankedPickList)
					break;
				else {
					product_recos.add(rankedPickList.get(customer_key)[i]);
					i++;
					count_recos++;
				}
			}
			System.out.println("Final RankedPickList Count: "+count_recos);
		}
		System.out.println("--hibridizer response generated");
		return product_recos;
	}
	
	// ###################################################################################
		// Get Genome Recommendations 
		
		public List<String> getProductGenomeRecos(String customer_key, int slot_num) {
			List<String> product_recos = new ArrayList<String>();
			if(customer_key.equals("-100") || genomeRecoList.get(customer_key) == null) {
				if(genomeRecoList.get(customer_key) == null){
					System.out.println(customer_key + " is not present in DB");
				}
				// Adding Trending in case of null customer
				int i=0;
				int count_trending=0;
				while(genomeRecoList.get("-100")[i]!= null) {
					if(count_trending==slot_num)
						break;
					else if(product_recos.contains(genomeRecoList.get("-100")[i])) {
						i++;
						continue;
					}else {
						product_recos.add(genomeRecoList.get("-100")[i]);
						i++;
						count_trending++;
					}
				}
				System.out.println("Trending: "+Arrays.toString(product_recos.toArray()));
			}
			// When customer Present in the system 
			else {
				int no_of_rankedPickList = slot_num;
				System.out.println(no_of_rankedPickList+" - "+(slot_num - no_of_rankedPickList));
				int i=0;
				int count_recos=0;
				while(genomeRecoList.get(customer_key)[i]!=null) {
					if(count_recos==no_of_rankedPickList)
						break;
					else {
						product_recos.add(genomeRecoList.get(customer_key)[i]);
						i++;
						count_recos++;
					}
				}
				System.out.println("Final GenomeList Count: "+count_recos);
				System.out.println("Genome-List: "+Arrays.toString(product_recos.toArray()));
			}
			return product_recos;
		}
		
		public List<String> getProductRecosIBCF(String product_key, int slot_num) {
			List<String> product_recos = new ArrayList<String>();
			
			//Customer_key is null or customer_key not present in IBCF
			if(product_key.equals("-100") || ibcfRecoList.get(product_key) == null) {
				return product_recos;
			}					
			//Adding product recos based on IBCF
			int no_of_ibcf = slot_num;
			int i=0;
			int count_recos=0;
			while(ibcfRecoList.get(product_key)[i]!=null) {
				if(count_recos==no_of_ibcf)
					break;
				else {
					//System.out.println(rankedPickList.get(customer_key)[i]);
					product_recos.add(ibcfRecoList.get(product_key)[i]);
					i++;
					count_recos++;
				}
			}
			System.out.println("Final IBCF Count: "+count_recos);
			System.out.println("IBCF: "+Arrays.toString(product_recos.toArray()));
			return product_recos;
		}

		public List<String> getProductRecosPreference(String customer_key, int slot_num) {
			List<String> product_recos = new ArrayList<String>();
			//Customer_key is null or customer_key not present in Preference
			if(customer_key.equals("-100") || preferenceRecoList.get(customer_key) == null) {
				return product_recos;
			}					
			//Adding product recos based on Preference
			int no_of_preference = slot_num;
			int i=0;
			int count_recos=0;
			while(preferenceRecoList.get(customer_key)[i]!=null) {
				if(count_recos==no_of_preference)
					break;
				else {
					//System.out.println(rankedPickList.get(customer_key)[i]);
					product_recos.add(preferenceRecoList.get(customer_key)[i]);
					i++;
					count_recos++;
				}
			}
			System.out.println("Final Preference Count: "+count_recos);
			System.out.println("Preference: "+Arrays.toString(product_recos.toArray()));
			return product_recos;
		}
		
		public String getQueriedCustomers(String loyalty_id){
			Map<String, Object> response = new HashMap<>();
			List<String> prod_id_list = new ArrayList<String>();
			String Customer_Id="";
			try{
				Customer_Id=(String)sql_data_structure_l.get(loyalty_id);
				}
			catch(Exception ex) {
				return "";
			}
			return Customer_Id;
		}
		
		public Map<String, Object> getLandingPageList(String bitly_code){
			Map<String, Object> response = new HashMap<>();
			List<String> prod_id_list = new ArrayList<String>();
			String Customer_Reco_Info="";
			try{
				String full_name="";
				String email="";
				String mobile="";
				String recos="";
				String value_1=landing_page_data.get(bitly_code).toString();
				System.out.println("Printing value_1");
				System.out.println(value_1);
				String[] list_1=value_1.split("\\|");
				if(list_1.length<4 ) {
					recos=list_1[-1];
				}
				else {
					full_name=list_1[0];
					email=list_1[1];
					mobile=list_1[2];
					recos=list_1[3];
				}
				System.out.println("full_name"+full_name);
				System.out.println("email"+email);
				System.out.println("mobile"+mobile);
				System.out.println("recos"+recos);
				// cpi.Full_Name,cpi.Email,cpi.Mobile,PN_Message
				
				String[] list_2=recos.split(",");
				List<Map<String,Object>> item_list = new ArrayList<Map<String,Object>>();
				for(String i:list_2) {
					Map<String,Object> inner_map = new HashMap();
					inner_map.put("Product_Variant_Id",i);
					item_list.add(inner_map);
				}
				response.put("Full_Name",full_name );
				response.put("Email",email );
				response.put("Customer_Key",mobile );
				List<Map<String,Object>> item_list_1 = new ArrayList<Map<String,Object>>();
				response.put("Customer_Key",mobile );
				response.put("items", item_list);
				}
			catch(Exception ex) {
				response.put("Error", "Bitly_Code not Recommendations Present");
				return response;
			}
			return response;
		}
		
		public List<String> getCustRecos(String cust_id){
			List<String> response = new ArrayList<String>();
			try {
				String reco_l=(String)rpl_stories.get(cust_id);
				String[] recos_list =reco_l.split(",");
				for(String j:recos_list) {
					response.add((String)product_key_Name_map.get(j));
					}
			}
			catch (Exception ex) {
		        System.out.println("Exception: " + ex);
		        }
			return response;
		}

}
