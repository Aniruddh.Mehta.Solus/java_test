package com.solus.solus_url_shotener_service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;


@Entity
@Immutable
//@Table(name="Event_Execution_History")
//@Access(value=AccessType.FIELD)
public class EventExecutionHistory {
	@Id
	@Column(name="Microsite_URL")
	private String micrositeURL;  
	@Column(name="PN_Message")
	private String pNMessage;
	@Column(name="Customer_Key")
	private String customerKey;
	public   String getmicrositeURL(){ return  micrositeURL ;}   
	public   String getpNMessage(){ return  pNMessage ;}
	public   String getcustomerKey(){ return  customerKey ;}
//	public  void setpNMessage( String pNMessage ){ this.pNMessage=pNMessage;} 
//	public  void setmicrositeURL( String micrositeURL ){ this.micrositeURL=micrositeURL;}
//	public  void setmicrositeURL( String micrositeURL ){ this.micrositeURL=micrositeURL;}
}



//@Entity
//@Immutable
//public class EventMaster {
//	@Id
//	@Column(name="ID")
//	private long id;                                                  
//	@Column(name="ChannelName")
//	private String channel;                                            
//	@Column(name="Website_URL")
//	private String websiteurl;                                          
//	@Column(name="Name")
//	private String name;                              
//	public   long getid(){ return  id ;}                                                                                      
//	public   String getchannel(){ return  channel ;}                                         
//	public   String getwebsiteurl(){ return  websiteurl ;}                             
//	public  String getname(){ return  name ;}                            
//}
