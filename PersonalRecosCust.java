package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.Solus_Microservice.reco_API.entity.RecosCust;

public interface PersonalRecosCust extends JpaRepository<RecosCust,String>{
	@Query(value="select Recos,Customer_Key from RankedPickList_Stories a, RankedPickList_Stories_Master b, CDM_Customer_Key_Lookup c where c.Customer_Id=a.Customer_Id and b.Story_Id=a.Story_Id and b.UseCaseKey=:story_key and c.Customer_Key=:cust_key  ", nativeQuery=true)
	public List<RecosCust> getCustomerRecos(@Param("cust_key") String customer_key,@Param("story_key") String reco_type );
}
