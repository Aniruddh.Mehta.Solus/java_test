package com.Solus_Microservice.reco_API.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.Solus_Microservice.reco_API.filter.JwtFilter;
import com.Solus_Microservice.reco_API.service.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration   extends WebSecurityConfigurerAdapter{
	@Autowired
    private UserService userService;

    @Autowired
    private JwtFilter jwtFilter;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(userService);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/solus_hybridizer_recos").permitAll()
                .antMatchers("/shopify_hybridizer_recos").permitAll()
                .antMatchers("/solus_genome_recos").permitAll()
                .antMatchers("/shopify_genome_recos").permitAll()
                .antMatchers("/solus_ibcf_recos").permitAll()
                .antMatchers("/solus_recogateway_ct").permitAll()
                .antMatchers("/solus_landingpage_recos").permitAll()
                .antMatchers("/solus_rcorestory_grouprecos").permitAll()
                .antMatchers("/api/solus/callback_localtext_delrep").permitAll()
                .antMatchers("/api/solus/callback_textlocal_sms_delrep").permitAll()
                .antMatchers("/solus_rcorestory_recos").permitAll()
                .antMatchers("/solus_preference_recos").permitAll()
                .antMatchers("/get_personalized_recos").permitAll()
                .antMatchers("/get_personalized_recos_2").permitAll()
                .antMatchers("/get_personalized_recos_for_batch").permitAll()
                .antMatchers("/get_copurchase_recos_for_product").permitAll()
                .antMatchers("/get_copurchase_recos_for_productlist").permitAll()
                .antMatchers("/get_deals_recos_for_productlist").permitAll()
                .antMatchers("/get_trending_recos").permitAll()
                .antMatchers("/get_last_order").permitAll()
                .antMatchers("/dummy_stock_values").permitAll()
                .antMatchers("/fetch_loyalty_ids").permitAll()
                .antMatchers("/solus_getLastMessage").permitAll()
                .antMatchers("/get_customer_display_name").permitAll()
                .antMatchers("/get_anonymized_contextual_recos").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers("/redirectWithRedirectView").permitAll()
                .antMatchers("/redirect").permitAll()
                .antMatchers("/get_custName").permitAll()
                .antMatchers("/get_CompDesc").permitAll()
                .antMatchers("/get_ProductPageURL").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);

    }
}
