package com.solus.solus_url_shotener_service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
public class Band2Recos {
	@Id
	@Column(name="Recos")
	private String recos;
	public  String getrecos(){ return  recos ;}
	
	@Column(name="Product_Name")
	private String productId;
	public  String getProductId(){ return  productId ;}
	
	@Column(name="LOV_Value")
	private String lovValue;
	public  String getlovValue(){ return  lovValue ;}
	
	@Column(name="Bought")
	private String boughtValue;
	public  String getboughtValue(){ return  boughtValue ;}
	
	@Column(name="Watchlist")
	private String watchlistValue;
	public  String getwatchlistValue(){ return  watchlistValue ;}
	
	@Column(name="CMOTS_COCODE")
	private String CMOTS_COCODE;
	public  String getCMOTS_COCODEValue(){ return  CMOTS_COCODE ;}
	
	@Column(name="ISIN")
	private String ISIN;
	public  String getISINValue(){ return  ISIN ;}
	
	@Column(name="EXCHANGEE")
	private String EXCHANGEE;
	public  String getEXCHANGEEValue(){ return  EXCHANGEE ;}
	
	@Column(name="ODINCODE")
	private String ODINCODE;
	public  String getODINCODEValue(){ return  ODINCODE ;}
	
}
