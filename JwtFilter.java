package com.Solus_Microservice.reco_API.filter;

import java.io.IOException;
import java.time.Instant;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.Solus_Microservice.reco_API.RecoApiApplication;
import com.Solus_Microservice.reco_API.config.AuthenticationExceptionHandler;
import com.Solus_Microservice.reco_API.service.JWTUtility;
import com.Solus_Microservice.reco_API.service.UserService;


@Component
public class JwtFilter extends OncePerRequestFilter {
    private String logLevel = RecoApiApplication.logLevel;
    @Autowired 
    private AuthenticationExceptionHandler authExHandler;
    
    @Autowired
    private JWTUtility jwtUtility;

    @Autowired
    private UserService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
	    	String authorization = httpServletRequest.getHeader("Authorization");
	        String token = null;
	        String userName = null;
	
	        if(null != authorization && authorization.startsWith("Bearer=")) {
	            token = authorization.substring(7);
	            userName = jwtUtility.getUsernameFromToken(token);
	        }
	
	        if(null != userName && SecurityContextHolder.getContext().getAuthentication() == null) {
	            UserDetails userDetails
	                    = userService.loadUserByUsername(userName);
	
	            if(jwtUtility.validateToken(token,userDetails)) {
	                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
	                        = new UsernamePasswordAuthenticationToken(userDetails,
	                        null, userDetails.getAuthorities());
	
	                usernamePasswordAuthenticationToken.setDetails(
	                        new WebAuthenticationDetailsSource().buildDetails(httpServletRequest)
	                );
	
	                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
	            }
	
	        }
	        filterChain.doFilter(httpServletRequest, httpServletResponse);
        }catch(Exception e) {
        	System.out.println("My exception: "+e);
    		if(logLevel.equals("TEST"))
				System.out.println(Instant.now() + " | ERROR | " + e);	
            authExHandler.commence(httpServletRequest, httpServletResponse, null);
        }
    }

}
