package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable

public class RecosCust {
	@Column(name="Recos")
	private String recos;
	public   String getrecos(){ return  recos ;}

	@Id
	@Column(name="Customer_Key")
	private String customer_key;
	public   String getcustomerKey(){ return  customer_key ;}
}
