package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.Solus_Microservice.reco_API.entity.CustFirstName;

public interface FirstNameRepo extends JpaRepository<CustFirstName,String>{
	@Query(value=" select COALESCE(First_Name,Full_Name) as Name from CDM_Customer_PII_Master a,CDM_Customer_Key_Lookup b where a.Customer_Id=b.Customer_Id and Customer_Key=:cust_key ", nativeQuery=true)
	public List<CustFirstName>getFirstNameFromKey(@Param("cust_key") String customerkey);
}
