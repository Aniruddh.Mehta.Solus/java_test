package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.Solus_Microservice.reco_API.entity.LandingPageRecos;

public interface LandingPageRecoRepo extends JpaRepository<LandingPageRecos,String>{
	@Query(value=" select eeh.Microsite_URL,cpi.Full_Name,cpi.Email,cpi.Mobile,PN_Message from Event_Execution_History eeh, CDM_Customer_PII_Master cpi where	 cpi.Customer_Id=eeh.Customer_Id and  eeh.Microsite_URL=:url_key ", nativeQuery=true)
	public List<LandingPageRecos> getLandingPageRecoList(@Param("url_key") String microsite_url);
}
