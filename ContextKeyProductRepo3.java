package com.solus.solus_url_shotener_service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.solus.solus_url_shotener_service.entity.Band3Recos;

public interface ContextKeyProductRepo3 extends JpaRepository<Band3Recos,String>{
	@Query(value="select Recos from RankedPickList_Stories a, RankedPickList_Stories_Master b, CDM_Customer_Key_Lookup c where c.Customer_Id=a.Customer_Id and b.Story_Id=a.Story_Id and b.UseCaseKey='UC001.Hybridizer.Reorder' and c.Customer_Key=:cust_key", nativeQuery=true)
	public List<Band3Recos>getContextProductList(@Param("cust_key") String customer_key);
}
