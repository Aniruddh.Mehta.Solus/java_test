package com.solus.solus_url_shotener_service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.solus.solus_url_shotener_service.entity.EventExecutionHistory;
import com.solus.solus_url_shotener_service.entity.RouteState;

public interface EventExecutionHistoryRepository extends JpaRepository<EventExecutionHistory,Long>{
	@Query(value="select Microsite_URL,PN_Message,Customer_Key  from Event_Execution_History,CDM_Customer_Key_Lookup where Event_Execution_History.Customer_Id=CDM_Customer_Key_Lookup.Customer_Id and Microsite_URL=:bitly_code ", nativeQuery=true)
	public List<EventExecutionHistory> getRecoEnum(@Param("bitly_code") String bitly_code);
}
//select Microsite_URL,PN_Message,Customer_Key  from Event_Execution_History,CDM_Customer_Key_Lookup where Event_Execution_History.Customer_Id=CDM_Customer_Key_Lookup.Customer_Id and Microsite_URL=
//select Microsite_URL,PN_Message  from Event_Execution_History where Microsite_URL=