package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
public class CustomerReco {

	@Id
	@Column(name="Customer_Key")
	private String customer_key;
	
	@Column(name="recos")
	private String recos;
	
	public   String getcustomerKey(){ return  customer_key ;}
	public   String getrecos(){ return  recos ;}
	
}
