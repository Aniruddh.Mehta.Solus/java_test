package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.Solus_Microservice.reco_API.entity.LastMessageSent;

@Repository
public interface LastMessageRepo extends JpaRepository<LastMessageSent,String>{
	@Query(value=" select replace(Description,' ','%20') as message from Event_Master where  EventId =(select Event_ID from Event_Execution_History where Event_Execution_ID = (select Event_Execution_ID from Event_Execution_History where Customer_Id= (select Customer_Id from CDM_Customer_Key_Lookup where Customer_Key=:cust_key ) order by Event_Execution_ID desc limit 1)) ", nativeQuery=true)
	public List<LastMessageSent> getLastMessageDetails(@Param("cust_key") String Customer_Key);
}

// select replace(Description,' ','%20') as message from Event_Master where  EventId =(select Event_ID from Event_Execution_History where Event_Execution_ID = (select Event_Execution_ID from Event_Execution_History where Customer_Id= (select Customer_Id from CDM_Customer_Key_Lookup where Customer_Key=:cust_key ) order by Event_Execution_ID desc limit 1))