package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
public class CoPurchaseProducts {
	
	@Column(name="Object_Id")
	private String objectId;
	public  String getobjectId(){ return  objectId ;}

	@Id
	@Column(name="Recos")
	private String recos;
	public  String getrecos(){ return  recos ;}
	
}
