package com.solus.solus_url_shotener_service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.solus.solus_url_shotener_service.entity.EventMaster;
import com.solus.solus_url_shotener_service.entity.RouteState;

public interface EventMasterRepository   extends JpaRepository<EventMaster,Long>{
	@Query(value=" select ID, ChannelName, Website_URL, Name, replace(Description,' ','%20') as Description from Event_Master where ID=:event_id ", nativeQuery=true)
//	@Query(value="SELECT  CLM_Campaign_Events.EventId AS ID, CLM_Channel.ChannelName AS ChannelName, CLM_Creative.Website_URL AS Website_URL,  CONCAT(CLM_Campaign_Trigger.CampTriggerName) AS Name, replace(CLM_MicroSegment.MicroSegmentName,' ','%20') AS Description  FROM CLM_Creative,  CLM_Channel,  CLM_Campaign_Events, CLM_Campaign_Trigger, CLM_MicroSegment  WHERE   CLM_Campaign_Events.ChannelId = CLM_Channel.ChannelId  AND CLM_Campaign_Events.CampTriggerId = CLM_Campaign_Trigger.CampTriggerId  AND CLM_Campaign_Events.CreativeId = CLM_Creative.CreativeId  AND CLM_Campaign_Events.EventId =:event_id AND CLM_Campaign_Events.MicroSegmentId = CLM_MicroSegment.MicroSegmentId GROUP BY CLM_Campaign_Events.EventId ", nativeQuery=true)
//	@Query(value="SELECT CLM_Campaign_Events.EventId AS ID, CLM_Channel.ChannelName AS ChannelName, CLM_Creative.Website_URL AS Website_URL, CONCAT(CLM_Campaign_Trigger.CampTriggerName) AS Name , CLM_MicroSegment.MicroSegmentName AS Description FROM         CLM_Creative,CLM_Channel,CLM_Campaign_Events,CLM_Campaign_Trigger WHERE  CLM_Campaign_Events.ChannelId = CLM_Channel.ChannelId  AND CLM_Campaign_Events.CampTriggerId = CLM_Campaign_Trigger.CampTriggerId  AND CLM_Campaign_Events.CreativeId = CLM_Creative.CreativeId AND CLM_Campaign_Events.EventId=:event_id  GROUP BY CLM_Campaign_Events.EventId ", nativeQuery=true)
	public List<EventMaster> getEventDetails(@Param("event_id") long event_id);


}
