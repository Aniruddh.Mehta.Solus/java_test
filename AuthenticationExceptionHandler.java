package com.Solus_Microservice.reco_API.config;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class AuthenticationExceptionHandler implements AuthenticationEntryPoint {
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			org.springframework.security.core.AuthenticationException authException)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		
    	Map<String, Object> err_response = new HashMap<String, Object>();
        err_response.put("code",108);
        err_response.put("desc","ACCOUNT_INVALID_CREDENTIALS"); 
        err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
        ObjectMapper mapper = new ObjectMapper();
        String err_json_response = mapper.writeValueAsString(err_response);
		
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getOutputStream().println(err_json_response);
	}
}
