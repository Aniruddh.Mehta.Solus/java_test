package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.Solus_Microservice.reco_API.entity.CustomerReco;

public interface CustomerRecoCheck extends JpaRepository<CustomerReco,String>{
	@Query(value="select c.Customer_Key,b.recos from RankedPickList_Stories_Master a, RankedPickList_Stories b, CDM_Customer_Key_Lookup c where a.Story_Id=b.Story_Id and c.Customer_Key in (:cust_list) and a.Name=:storyName and c.Customer_Id=b.Customer_Id   ", nativeQuery=true)
	public List<CustomerReco> getCustomerRecoList(@Param("cust_list") List<String> customer_list,@Param("storyName") String storyName  );
}
