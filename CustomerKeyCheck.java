package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.Solus_Microservice.reco_API.entity.CustomerKey;

public interface CustomerKeyCheck  extends JpaRepository<CustomerKey,String>{
	@Query(value="select Customer_Key from CDM_Customer_Key_Lookup where Customer_Key in (:cust_list)  ", nativeQuery=true)
	public List<CustomerKey> getCustomerKeyList(@Param("cust_list") List<String> customer_list );
}
