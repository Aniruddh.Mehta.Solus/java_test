package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable

public class ShopifyGenomeProductList {
	@Id
	@Column(name="Product_Variant_Id")
	private String productvariantId;
	public  String getproductvariantId(){ return  productvariantId ;}

	@Column(name="Product_Id")
	private String productId;
	public  String getproductId(){ return  productId ;}

	@Column(name="Product_Handle")
	private String productHandle;
	public  String getproductHandle(){ return  productHandle ;}
}
