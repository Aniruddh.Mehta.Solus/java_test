package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.Solus_Microservice.reco_API.entity.RcoreStoryPoint;
@Repository
public interface RcoreStoryCheck extends JpaRepository<RcoreStoryPoint,String> {
	@Query(value="select Name from RankedPickList_Stories_Master where Name=:storyname  ", nativeQuery=true)
	public List<RcoreStoryPoint> getStoryPoint(@Param("storyname") String storyName);
}
