package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
public class GateWayRecos {
	@Id
	@Column(name="Recos")
	private String recos;
	public   String getGateWayRecos(){ return  recos ;}
}
