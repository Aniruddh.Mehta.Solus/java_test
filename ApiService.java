package com.Solus_Microservice.reco_API.service;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.Solus_Microservice.reco_API.entity.AnonRecoTypeCheck;
import com.Solus_Microservice.reco_API.entity.CoPurchaseProducts;
import com.Solus_Microservice.reco_API.entity.ContextKeyCheck;
import com.Solus_Microservice.reco_API.entity.ContextKeyProducts;
import com.Solus_Microservice.reco_API.entity.CustFirstName;
import com.Solus_Microservice.reco_API.entity.CustomerKey;
import com.Solus_Microservice.reco_API.entity.CustomerReco;
import com.Solus_Microservice.reco_API.entity.GateWayRecos;
import com.Solus_Microservice.reco_API.entity.LandingPageRecos;
import com.Solus_Microservice.reco_API.entity.LastMessageSent;
import com.Solus_Microservice.reco_API.entity.LastOrderProducts;
import com.Solus_Microservice.reco_API.entity.Product_Info;
import com.Solus_Microservice.reco_API.entity.RcoreStory;
import com.Solus_Microservice.reco_API.entity.RcoreStoryPoint;
import com.Solus_Microservice.reco_API.entity.RecoTypeCheck;
import com.Solus_Microservice.reco_API.entity.RecosCust;
import com.Solus_Microservice.reco_API.entity.ShopifyGenomeProductList;
import com.Solus_Microservice.reco_API.entity.ShopifyHybridProductList;
import com.Solus_Microservice.reco_API.entity.TrendingProducts;
import com.Solus_Microservice.reco_API.repository.AnonRecoTypeRepo;
import com.Solus_Microservice.reco_API.repository.CoPurchaseRecoList;
import com.Solus_Microservice.reco_API.repository.ContextKeyCheckRepo;
import com.Solus_Microservice.reco_API.repository.ContextKeyProductRepo;
import com.Solus_Microservice.reco_API.repository.CustomerKeyCheck;
import com.Solus_Microservice.reco_API.repository.CustomerRecoCheck;
import com.Solus_Microservice.reco_API.repository.FirstNameRepo;
import com.Solus_Microservice.reco_API.repository.GateWayRecosRepo;
import com.Solus_Microservice.reco_API.repository.LandingPageRecoRepo;
import com.Solus_Microservice.reco_API.repository.LastMessageRepo;
import com.Solus_Microservice.reco_API.repository.LastOrderProductRepo;
import com.Solus_Microservice.reco_API.repository.PersonalRecosCust;
import com.Solus_Microservice.reco_API.repository.PersonalRecosCustList;
import com.Solus_Microservice.reco_API.repository.ProductInfoRepo;
import com.Solus_Microservice.reco_API.repository.RcoreStoryCheck;
import com.Solus_Microservice.reco_API.repository.RcoreStoryRepo;
import com.Solus_Microservice.reco_API.repository.RecoTypeRepo;
import com.Solus_Microservice.reco_API.repository.ShopifyGenomeProductRepo;
import com.Solus_Microservice.reco_API.repository.ShopifyHybridProductRepo;
import com.Solus_Microservice.reco_API.repository.TrendingProductsRepo;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service
public class ApiService {
	// ### Constant Input Values
	
	@Value("${reco.count}")
	private String recoCount;
	@Value("${ibcf.reco.count}")
	private String ibcfRecoCount;
	@Value("${server.host}")
	private String server_host;
	@Value("${data-ingestion.client}")
	private String client_name;
	@Value("${data-ingestion.output.filepath}")
	private String output_filepath;
	@Value("${data-ingestion.file.max-request}")
	private String max_request_per_file;
	@Value("${microservice.domain}")
	private String api_domain;
	private long request_count=0;
	private long awssns_delrep_request_count=2000;
	private long localtext_delrep_request_count=2000;
	private long textlocal_sms_delrep_request_count=2000;
	private long products_landing_page_count=2000;
	private long products_smartbasket_count=2000;
	private String output_filename = ""; 
	private String filename_date = (new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()))
			+"_";
	@Value("${data-intelligence.bulk_output.filepath}")
	private String bulkOutputFolder;
	@Value("${data-intelligence.bulk_output.batch_size}")
	private String output_batch_size;
	
	
	
	@Autowired
	private FileReaderUtility file;
	@Autowired
	private RcoreStoryRepo rcoreRepo;
	@Autowired
	private RcoreStoryCheck rcoreStoryChk;
	@Autowired 
	private CustomerKeyCheck custkeychck;
	@Autowired 
	private CustomerRecoCheck custrecochck;
	@Autowired 
	private RecoTypeRepo typeRepo;
	@Autowired 
	private AnonRecoTypeRepo typeAnonRepo;
	@Autowired 
	private ProductInfoRepo productInfoRepo;
	@Autowired
	private PersonalRecosCust personal_recos_cust;
	@Autowired
	private PersonalRecosCustList personal_recos_cust_list;
	@Autowired
	private CoPurchaseRecoList copurchaseproductlist ;
	@Autowired 
	private TrendingProductsRepo trendingProRepo;
	@Autowired
	private LastOrderProductRepo lastOrderProductRepo;
	@Autowired 
	private ContextKeyCheckRepo contextKeychckrepo;
	@Autowired 
	private ContextKeyProductRepo contxtkeyProdrepo;
	@Autowired 
	private ShopifyHybridProductRepo shopifyHydProductrepo;
	@Autowired
	private ShopifyGenomeProductRepo shopifygenProductRepo;
	@Autowired
	private LandingPageRecoRepo landingPagerecos;
	@Autowired
	private LastMessageRepo lastmsgRepo;
	@Autowired
	private GateWayRecosRepo gatewayRepo;
	@Autowired 
	private FirstNameRepo firstnameRep;
	// #################################################################################################
	// Hybrididizer Recommendations
	public Map<String, Object> getHybridizerReco(Map<String, Object> request) {
		request.put("slot_num", Integer.parseInt(recoCount)); //Extra recos incase some products are unavailable at store
		List<String> productList = file.getProductRecosHybridizer(request.get("Customer_email").toString(), Integer.parseInt(request.get("slot_num").toString()));
		//Preparing product details from product_key
		List<Map> final_list = new ArrayList<Map>();
		Map<String, String> product_details = new HashMap<String, String>();
		for(int i=0; i< productList.size(); i++)
		{
			try {
				product_details = file.getProductData(productList.get(i));
				final_list.add(product_details);
				}
				catch (Exception e) {
					System.out.println("WARNING: PRODUCT DETAILS NOT FOUND IN THE WEBSITE for Hybridizer "+productList.get(i).toString());
				}
		}		
		//Preparing response
		Map<String, Object> response = new HashMap<>();
		response.put("Customer_email", request.get("Customer_email"));
		response.put("items", final_list);
		return response ;
	}

//	 "Product_Handle": "carrot-500g",
//     "Product_Id": "4485113577558",
//     "Product_Variant_Id": "31705949569110"
	// Hybrididizer Recommendations
		public Map<String, Object> getShopifyHybridizerReco(Map<String, Object> request) {
			request.put("slot_num", Integer.parseInt(recoCount)); //Extra recos incase some products are unavailable at store
			String cust_email = (String)request.get("Customer_email");
			List<ShopifyHybridProductList> shopHyList = shopifyHydProductrepo.getShopifyProductList(cust_email);
			List<Map> final_list = new ArrayList<Map>();
			Map<String, Object> response = new HashMap<>();
			Map<String, String> product_details = new HashMap<String, String>();
			for(int i=0;i<shopHyList.size();i++) {
				product_details.put("Product_Handle",shopHyList.get(i).getproductHandle());
				product_details.put("Product_Id",shopHyList.get(i).getproductId());
				product_details.put("Product_Variant_Id",shopHyList.get(i).getproductvariantId());
				final_list.add(product_details);
			}
			response.put("Customer_email", request.get("Customer_email"));
			response.put("items", final_list);
			return response ;
		}
		
		public Map<String, Object> getShopifyGenomeReco(Map<String, Object> request) {
			request.put("slot_num", Integer.parseInt(recoCount)); //Extra recos incase some products are unavailable at store
			String cust_email = (String)request.get("Customer_email");
			List<ShopifyGenomeProductList> shopHyList = shopifygenProductRepo.getShopifyGenomeProductList(cust_email);
			List<Map> final_list = new ArrayList<Map>();
			Map<String, Object> response = new HashMap<>();
			Map<String, String> product_details = new HashMap<String, String>();
			for(int i=0;i<shopHyList.size();i++) {
				product_details.put("Product_Handle",shopHyList.get(i).getproductHandle());
				product_details.put("Product_Id",shopHyList.get(i).getproductId());
				product_details.put("Product_Variant_Id",shopHyList.get(i).getproductvariantId());
				final_list.add(product_details);
			}
			response.put("Customer_email", request.get("Customer_email"));
			response.put("items", final_list);
			return response ;
		}
	// #################################################################################################
	// Genome Recommendations
	public Map<String, Object> getGenomeReco(Map<String, Object> request) {
		request.put("slot_num", Integer.parseInt(recoCount)); //Extra recos incase some products are unavailable at store
		List<String> productList = file.getProductGenomeRecos(request.get("Customer_email").toString(), Integer.parseInt(request.get("slot_num").toString()));
		//Preparing product details from product_key
		List<Map> final_list = new ArrayList<Map>();
		Map<String, String> product_details = new HashMap<String, String>();
		for(int i=0; i< productList.size(); i++)
		{
			try {
				product_details = file.getProductData(productList.get(i));
				final_list.add(product_details);
				}
				catch (Exception e) {
					System.out.println("WARNING: PRODUCT DETAILS NOT FOUND IN THE WEBSITE for Hybridizer "+productList.get(i).toString());
				}
		}		
		//Preparing response
		Map<String, Object> response = new HashMap<>();
		response.put("Customer_email", request.get("Customer_email"));
		response.put("items", final_list);
		return response ;
	}
	
	// #################################################################################################
	// IBCF Recommendations
	public Map<String, Object> getIBCFReco(Map<String, Object> request) {
		//User will always receive x recommendations
		request.put("slot_num", Integer.parseInt(ibcfRecoCount)); //Extra recos incase some products are unavailable at store
		List<String> productList = file.getProductRecosIBCF(request.get("Variant_Id").toString(), Integer.parseInt(request.get("slot_num").toString()));
		List<Map> final_list = new ArrayList<Map>();
		Map<String, String> product_details = new HashMap<String, String>();
		for(int i=0; i< productList.size(); i++)
		{
			try {
				product_details = file.getProductData(productList.get(i));
				final_list.add(product_details);
				}
				catch (Exception e) {
					System.out.println("WARNING: PRODUCT DETAILS NOT FOUND IN THE WEBSITE for IBCF  "+productList.get(i).toString());
				}
		}
		Map<String, Object> response = new HashMap<>();
		response.put("Variant_Id", request.get("Variant_Id"));
		response.put("items", final_list);
		return response;	
	}
	// #################################################################################################
	// RCORE Stories Recommendations
	public Map<String, Object> getRcoreStoryReco(Map<String, Object> request) {
		request.put("slot_num", Integer.parseInt(recoCount)); //Extra recos incase some products are unavailable at store
		List<Map> final_list = new ArrayList<Map>();
		Map<String, Object> response = new HashMap<>();
		String storyName = request.get("Story_Name").toString();
		String customer_key = request.get("Customer_Key").toString();
		List<RcoreStory> storyList = rcoreRepo.getStoryDetails(storyName,customer_key);
		System.out.println(storyList.get(0).getname());
		System.out.println(storyList.get(0).getstoryId());
		System.out.println(storyList.get(0).getrecos());
		System.out.println(storyList.get(0).getcustomerKey());
		response.put("Story_Name", request.get("Story_Name"));
		response.put("Customer_Key", request.get("Customer_Key"));
		Map<String, String> product_details = new HashMap<String, String>();
	    String[] tokens=storyList.get(0).getrecos().split(",");  
		for(int i=0; i< tokens.length; i++)
		{
			try {
				product_details.put("Product_Variant_Id", tokens[i]);
				final_list.add(product_details);
				}
				catch (Exception e) {
					System.out.println("WARNING: PRODUCT DETAILS NOT FOUND ");
				}
		}
		response.put("items", final_list);
		return response;	
	}
	// #################################################################################################
	// Preferences Recommendations
	public Map<String, Object> getPreferenceReco(Map<String, Object> request) {
		request.put("slot_num", Integer.parseInt(recoCount)); //Extra recos incase some products are unavailable at store
		List<String> productList = file.getProductRecosPreference(request.get("Customer_email").toString(), Integer.parseInt(request.get("slot_num").toString()));
		List<Map> final_list = new ArrayList<Map>();
		Map<String, String> product_details = new HashMap<String, String>();
		for(int i=0; i< productList.size(); i++)
		{
			try {
				product_details = file.getProductData(productList.get(i));
				final_list.add(product_details);
				}
				catch (Exception e) {
					System.out.println("WARNING: PRODUCT DETAILS NOT FOUND IN THE WEBSITE for Preference "+productList.get(i).toString());
				}
		}
		
		Map<String, Object> response = new HashMap<>();
		response.put("Customer_email", request.get("Customer_email"));
		response.put("items", final_list);
		return response;	
	}
	
	// #################################################################################################
	// Delrep Insertions
	public Map<String, Object> putData(Map<String, Object> inputData, String filetype) {
		Map<String, Object> response = new HashMap<String, Object>();
		// Putting Delrep Count values in for different filetype values 
		long req_count;
		if(filetype.equals("Callback_localtext_DelRep")) {
			req_count = localtext_delrep_request_count;
			output_filename="Callback_localtext_DelRep";
		}
		else if(filetype.equals("Callback_AWS-SNS_DelRep")) {
			req_count = awssns_delrep_request_count;
			output_filename="Callback_AWS-SNS_DelRep";
		}
		else if(filetype.equals("Callback_textlocal_sms_DelRep")) {
			req_count = textlocal_sms_delrep_request_count;
			output_filename="Callback_textlocal_sms_DelRep";
		}
		else if(filetype.equals("product_landing_page")) {
			req_count = products_landing_page_count;
			output_filename="product_landing_page";
		}
		else if(filetype.equals("product_smartbasket")) {
			req_count = products_smartbasket_count;
			output_filename="product_smartbasket";
		}
		else {
			req_count = request_count;
			output_filename="Callback_localtext_DelRep";
		}
		System.out.println("request_count="+request_count);
		System.out.println("client_name="+client_name);
		System.out.println("server_host="+server_host);
		// ############################################### Processing of File Creation ###################################################
	try{
		String host_server = server_host.replace(".", "-");
		Path path = Paths.get(output_filepath+output_filename);
		System.out.println("############################    Path=   "+path);
		// Max Liimit of request per file already fit in it 
		System.out.println(req_count+" "+Long.parseLong(max_request_per_file)+" "+(req_count >= Long.parseLong(max_request_per_file)));
		System.out.println("IF Condition Output Part 1="+(!output_filename.equals("")));
		System.out.println("IF Condition Output Part 2="+(req_count >= Long.parseLong(max_request_per_file)));
		
		//  #################################################################################
		//  Deciding Path Variable
		if(!output_filename.equals("") && req_count >= Long.parseLong(max_request_per_file)) {
			filename_date = (new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()))
	    			+"_";
			output_filename = filename_date + host_server + "_" + client_name + "_"+ filetype +".txt";
	    	path = Paths.get(output_filepath+output_filename);
	    	if(filetype.equals("Callback_localtext_DelRep")) {
				localtext_delrep_request_count = 0;
				output_filename="Callback_localtext_DelRep";
	    	}
			else if(filetype.equals("Callback_AWS-SNS_DelRep")) {
				awssns_delrep_request_count = 0;
				output_filename="Callback_AWS-SNS_DelRep";
			}
			else if(filetype.equals("Callback_textlocal_sms_DelRep")) {
				textlocal_sms_delrep_request_count = 0;
				output_filename="Callback_textlocal_sms_DelRep";
			}
			else if(filetype.equals("product_landing_page")) {
				products_landing_page_count=0;
				output_filename="product_landing_page";
			}
			else if(filetype.equals("product_smartbasket")) {
				products_smartbasket_count=0;
				output_filename="product_smartbasket";
			}
			else {
				request_count=0;
				output_filename="DelRep_Output";
			}
	    	System.out.println("if: "+path);
		}else {
			output_filename = filename_date + host_server+"_" + client_name + "_"+ filetype +".txt";
			path = Paths.get(output_filepath+output_filename);
			System.out.println("else: "+path);
		}
		Path pathParent = path.getParent();
	    if(!Files.exists(pathParent)) {
	    	Files.createDirectories(pathParent);
	    }
	    ObjectMapper objectMapper = new ObjectMapper();
	    String json_input = objectMapper.writeValueAsString(inputData);
	    String newjson_input = json_input.replace("\n", "|");
	    Files.write(path, Arrays.asList(newjson_input), StandardCharsets.UTF_8,
	        Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
	    if(filetype.equals("Callback_localtext_DelRep"))
			localtext_delrep_request_count++;
		else if(filetype.equals("Callback_AWS-SNS_DelRep"))
			awssns_delrep_request_count++;
		else if(filetype.equals("Callback_textlocal_sms_DelRep"))
			textlocal_sms_delrep_request_count++;
		else if(filetype.equals("Shopify_landing_page"))
			products_landing_page_count++;
		else if(filetype.equals("Shopify_smartbasket"))
			products_smartbasket_count++;
		else
			request_count++;
	    response.put("Status", "OK");
	}catch (final Exception e) {
	    // Add your own exception handling...
		System.out.println(e);
		response.put("Status", "Error - "+e);
		}	
		return response;
	}
	// #################################################################################################
	// Get Story State
	// #################
		public Map<String, Object> getStoryState(String storyName) {
			Map<String, Object> state_response = new HashMap<String, Object>();
			// Check Existence of Story
			try {
				List<RcoreStoryPoint> storyPointList = rcoreStoryChk.getStoryPoint(storyName);
				String storyPointName=storyPointList.get(0).getname();
				if(!storyPointName.equals("")) {
					// 1.
					// Create Customer Array
					
					// 2.
					// Create Product Array 
					state_response.put("Story_State","true");
				}
			}
			catch(Exception ex) {
				Map<String, Object> err_response = new HashMap<String, Object>();
	            err_response.put("desc","Invalid Rcore Story Name"); 
	            System.out.println(err_response);
	            return err_response;
		}
			return state_response;
		}
		// #################################################################################################
		// Get RCORE GROUP Reco Map
		// #################
			public Map<String, Object> getRcoreGroupRecoMap(List<String> customer_list, Integer  reco_count,String storyName) {
				Map<String, Object> response = new HashMap<String, Object>();
				response.put("Story_Name", storyName);
				response.put("Reco_Count", reco_count);
				response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
				Map<String, Object> response_input = new HashMap<String, Object>();
				// Storing only valid Customers inside secondary List
				List<String> customer_list_valid = new ArrayList<String>();
				System.out.println("Point 4:Before getting CustCheck"+new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").format(new Date()));
				List<CustomerKey> CustListRepoOut=custkeychck.getCustomerKeyList(customer_list);
				for(int j=0;j<CustListRepoOut.size();j++) {
					customer_list_valid.add(CustListRepoOut.get(j).getcustomerKey());
				}
				System.out.println("Point 5:Before getting CustRecoCheck"+new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").format(new Date()));
				List<CustomerReco> CustListOutputQuery = custrecochck.getCustomerRecoList(customer_list_valid,storyName);
				System.out.println("Point 6:After getting CustRecoCheck"+new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").format(new Date()));
				for(int k=0;k<CustListOutputQuery.size();k++) {
					Map<String, Object> cust_detail = new HashMap<String, Object>();
					List<String> reco_list = new ArrayList<String>();
					String[] recos = CustListOutputQuery.get(k).getrecos().split(",");
					for(int l=0;l<reco_count;l++) {
						reco_list.add(recos[l]);
					}
					//cust_detail.put(CustListOutputQuery.get(k).getcustomerKey(),reco_list);
					response_input.put(CustListOutputQuery.get(k).getcustomerKey(),reco_list);
				}
				System.out.println("Point 7:After getting Looping Response for CustRecoCheck"+new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").format(new Date()));
				// Insert Customers Not Part of the List 
				// Compare customer_list,customer_list_valid
				List<String> invalid_customers = new ArrayList<String>(customer_list);
				invalid_customers.removeAll(customer_list_valid);
				for(int m=0;m<invalid_customers.size();m++) {
					response_input.put(invalid_customers.get(m),"Customer Not Found");
				}
				response.put("Customer_Reco_List",response_input);
				return response;
			}
			// #################################################################################################
			// Get Reco-Check Map
			// #################
				public String checkRecoKey(String reco_type) {
					String response ="True";
					String responseOut ="";
					// CReate Entity for Check and return true if value present
					List<RecoTypeCheck> reco_name=typeRepo.getRecoTypeDetails(reco_type);
					System.out.println(reco_name);
					try {
						//System.out.println("UseCaseKey"+reco_name.get(0).getusecaseKey());
						//System.out.println("Story_Id"+reco_name.get(0).getStoryId());
						responseOut=reco_name.get(0).getusecaseKey();
					}catch(Exception ex) {
						return "False";
					}
					return response;
				}
				public Integer checkRecoStoryId(String reco_type) {
					String responseOut ="";
					// CReate Entity for Check and return true if value present
					List<RecoTypeCheck> reco_name=typeRepo.getRecoTypeDetails(reco_type);
					System.out.println(reco_name);
					try {
						responseOut=reco_name.get(0).getusecaseKey();
					}catch(Exception ex) {
						return -1;
					}
					return reco_name.get(0).getStoryId();
				}
				public Integer anonStoryCheck(String reco_type) {
					String responseOut ="";
					// CReate Entity for Check and return true if value present
					List<AnonRecoTypeCheck> reco_name=typeAnonRepo.getAnonRecoTypeDetails(reco_type);
					System.out.println(reco_name);
					try {
						responseOut=reco_name.get(0).getusecaseKey();
					}catch(Exception ex) {
						return -1;
					}
					return reco_name.get(0).getStoryId();
				}
				public Integer checkCustomerKey(String customer_key) {
					String responseOut ="";
					// Check if Customer Key Valid 
					List<String> customer_list= new ArrayList<String>();;
					customer_list.add(customer_key);
					List<CustomerKey> CustListRepoOut=custkeychck.getCustomerKeyList(customer_list);
					//List<RecoTypeCheck> reco_name=typeRepo.getRecoTypeDetails(reco_type);
					try {
						responseOut=CustListRepoOut.get(0).getcustomerKey();
					}catch(Exception ex) {
						return -1;
					}
					return 1;
				}
				public Integer checkContextKey(String context_key) {
					Integer responseOut =0; 
					List<ContextKeyCheck> ContextListRepoOut=contextKeychckrepo.getContectKeyList(context_key);
					try {
						responseOut=ContextListRepoOut.get(0).getStoryId();
					}catch(Exception ex) {
						return -1;
					}
					return responseOut;
				}
				public Integer checkCustomerKeyList(List<String> customer_key_list) {
					String responseOut ="";
					// Check if Customer Key Valid 
					List<String> customer_list= new ArrayList<String>();
					for(int i=0;i<customer_key_list.size();i++) {
						customer_list.add(customer_key_list.get(i));
					}
					List<CustomerKey> CustListRepoOut=custkeychck.getCustomerKeyList(customer_list);
					
					try {
						responseOut=CustListRepoOut.get(0).getcustomerKey();
					}catch(Exception ex) {
						return -1;
					}
					return customer_list.size();
				}
				public String getPersonalRecos(String customer_key,String reco_type,Integer reco_cnt) {
					Map<String, Object> response = new HashMap<>();
					// check if reco count greater the limit  
					if (Integer.parseInt(recoCount)<reco_cnt) {
						String inx="Given Reco_Count limit is more than allowed set of recommendations of "+recoCount+" recos; Please Connect the CS Team in regards to the Upper Limit default value";
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",127);
					    err_response.put("desc",inx);
					    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						System.out.println(Instant.now() + " | ERROR | " + "Reco_Count More than the specified Limit  ");	
					    return "NA";
					}
					StringBuilder output = new StringBuilder("");
					List<RecosCust> reco_list_unit =personal_recos_cust.getCustomerRecos(customer_key,reco_type);
					System.out.println(reco_list_unit.get(0).getrecos());
					String[] products= reco_list_unit.get(0).getrecos().split(",");
					List<String> product_list = new ArrayList<String>();
					for(int i=0; i< reco_cnt; i++)
					{
						//get product Info from product_key
						try {
							Map<String, Object> inner_prod_deets = new HashMap<String, Object>();
							//inner_prod_deets
							String productKey=products[i];
							List<Product_Info> prod_info_1 =productInfoRepo.getProductInfo_one(productKey);
							inner_prod_deets.put("Product_Id", prod_info_1.get(0).getProductId());
							inner_prod_deets.put("Product_Name", prod_info_1.get(0).getrecos());
							inner_prod_deets.put("Product_Sector", prod_info_1.get(0).getlovValue());
							inner_prod_deets.put("Bought", prod_info_1.get(0).getboughtValue());
							inner_prod_deets.put("Watchlist", prod_info_1.get(0).getwatchlistValue());
							inner_prod_deets.put("CMOTS_COCODE", prod_info_1.get(0).getISINValue());
							inner_prod_deets.put("ISIN", prod_info_1.get(0).getEXCHANGEEValue());
							inner_prod_deets.put("EXCHANGE", prod_info_1.get(0).getCMOTS_COCODEValue());
							inner_prod_deets.put("ODINCODE", prod_info_1.get(0).getODINCODEValue());

							StringBuilder sb = new StringBuilder("{");
							for (Map.Entry<String, Object> entry : inner_prod_deets.entrySet()) {
					            String key = entry.getKey();
					            Object value = entry.getValue();
					            sb.append("\"").append(key).append("\":\"");
					            if (value != null) {
					                sb.append(value.toString());
					            }
					            sb.append("\",");
					        }
					        sb.deleteCharAt(sb.length() - 1); // remove the last comma
					        sb.append("}");
					        output.append(sb);
				            output.append(",");
							product_list.add(products[i]);
							}
							catch (Exception e) {
								response.put("Error","Recos Not Found");
								return "NA";
							}
					}
					response.put("Product_List",product_list);
					return output.toString();
				}
				
				public Map<String, Object> getPersonalRecosBatch(List<String> customer_keys,String reco_type,Integer reco_cnt) {
					Map<String, Object> response = new HashMap<>();
					Map<String, Object> response_l = new HashMap<>();
					// check if reco count greater the limit  
					if (Integer.parseInt(recoCount)<reco_cnt) {
						String inx="Given Reco_Count limit is more than allowed set of recommendations of "+recoCount+" recos; Please Connect the CS Team in regards to the Upper Limit default value";
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",127);
					    err_response.put("desc",inx);
					    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						System.out.println(Instant.now() + " | ERROR | " + "Reco_Count More than the specified Limit  ");	
					    return err_response;
					}
					List<RecosCust> reco_lists_cust =personal_recos_cust_list.getCustomerListRecos(customer_keys,reco_type);
					System.out.println(reco_lists_cust.get(0).getrecos());
					//String[] products= reco_lists_cust.get(0).getrecos().split(",");
					for(int i=0;i<reco_lists_cust.size();i++) {
						String[] products= reco_lists_cust.get(i).getrecos().split(",");
						List<String> product_list = new ArrayList<String>();
						for(int j=0; j< products.length; j++) {
							try {
								product_list.add(products[j]);
								}
								catch (Exception e) {
									response.put("Error","Recos Not Found For This Customer");
									return response;
								}
						}
						response_l.put(reco_lists_cust.get(i).getcustomerKey(),product_list);
					}
					response.put("Customer_Reco_List", response_l);
					return response;
				}
				
				public Map<String, Object> getCopurchaseRecos(String product_key,Integer story_id,Integer reco_cnt) {
					Map<String, Object> response = new HashMap<>();
					// check if reco count greater the limit  
					if (Integer.parseInt(recoCount)<reco_cnt) {
						String inx="Given Reco_Count limit is more than allowed set of recommendations of "+recoCount+" recos; Please Connect the CS Team in regards to the Upper Limit default value";
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",127);
					    err_response.put("desc",inx);
					    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						System.out.println(Instant.now() + " | ERROR | " + "Reco_Count More than the specified Limit  ");	
					    return err_response;
					}
					List<CoPurchaseProducts> copurchaseList = copurchaseproductlist.getCoPurchaseProduct(product_key,story_id);
					System.out.println(copurchaseList.get(0).getrecos());
					List<String> product_list_1 = new ArrayList<String>();
					for (int i=0;i<copurchaseList.size() && i<reco_cnt;i++) {
						product_list_1.add(copurchaseList.get(i).getrecos());
					}
					response.put("Product_List", product_list_1);
					return response;
				}
				public Map<String, Object> getCopurchaseRecosList(List<String> product_key_list,Integer story_id,Integer reco_cnt) {
					Map<String, Object> response = new HashMap<>();
					// check if reco count greater the limit  
					if (Integer.parseInt(recoCount)<reco_cnt) {
						String inx="Given Reco_Count limit is more than allowed set of recommendations of "+recoCount+" recos; Please Connect the CS Team in regards to the Upper Limit default value";
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",127);
					    err_response.put("desc",inx);
					    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						System.out.println(Instant.now() + " | ERROR | " + "Reco_Count More than the specified Limit  ");	
					    return err_response;
					}
					List<String> product_list_1 = new ArrayList<String>();
					List<String> product_list_o = new ArrayList<String>();
					for(int i=0;i<product_key_list.size();i++) {
						String product_key = product_key_list.get(i);
						List<CoPurchaseProducts> copurchaseList = copurchaseproductlist.getCoPurchaseProduct(product_key,story_id);
						for (int j=0;j<copurchaseList.size() ;j++) {
							product_list_1.add(copurchaseList.get(j).getrecos());
						}
					}
					for(int i=0;i<product_list_1.size() && i<reco_cnt;i++) {
						product_list_o.add(product_list_1.get(i));
					}
					response.put("Product_List", product_list_o);					
					return response;
				}
				
				public Map<String, Object> getDealsRecosList(List<String> product_key_list,Integer story_id,Integer reco_cnt) {
					Map<String, Object> response = new HashMap<>();
					// check if reco count greater the limit  
					if (Integer.parseInt(recoCount)<reco_cnt) {
						String inx="Given Reco_Count limit is more than allowed set of recommendations of "+recoCount+" recos; Please Connect the CS Team in regards to the Upper Limit default value";
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",127);
					    err_response.put("desc",inx);
					    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						System.out.println(Instant.now() + " | ERROR | " + "Reco_Count More than the specified Limit  ");	
					    return err_response;
					}
					List<String> product_list_1 = new ArrayList<String>();
					List<String> product_list_o = new ArrayList<String>();
					for(int i=0;i<product_key_list.size();i++) {
						String product_key = product_key_list.get(i);
						List<CoPurchaseProducts> copurchaseList = copurchaseproductlist.getCoPurchaseProduct(product_key,story_id);
						for (int j=0;j<copurchaseList.size() ;j++) {
							product_list_1.add(copurchaseList.get(j).getrecos());
						}
					}
					for(int i=0;i<product_list_1.size() && i<reco_cnt;i++) {
						product_list_o.add(product_list_1.get(i));
					}
					response.put("Product_List", product_list_o);					
					return response;
				}
				
				public String getFirstName(String customerkey) {
					String output="";
					List<CustFirstName> first_name_list = firstnameRep.getFirstNameFromKey(customerkey);
					output=first_name_list.get(0).getName();
					return output ;
				}
				
				public String getTrendingRecosList(Integer story_id,Integer reco_cnt) {
					Map<String, Object> response = new HashMap<>();
					// check if reco count greater the limit  
					if (Integer.parseInt(recoCount)<reco_cnt) {
						String inx="Given Reco_Count limit is more than allowed set of recommendations of "+recoCount+" recos; Please Connect the CS Team in regards to the Upper Limit default value";
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",127);
					    err_response.put("desc",inx);
					    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						System.out.println(Instant.now() + " | ERROR | " + "Reco_Count More than the specified Limit  ");	
					    return "NA";
					}
					StringBuilder output = new StringBuilder("");
					List<String> product_list_1 = new ArrayList<String>();
					List<TrendingProducts> trendingProductList =trendingProRepo.getTrendingProducts(story_id);
					for(int i=0;i<trendingProductList.size() && i<reco_cnt;i++) {
						//product_list_1.add(trendingProductList.get(i).getrecos());
						Map<String, Object> inner_product_deets = new HashMap<>();
						
						inner_product_deets.put("Product_Id", trendingProductList.get(i).getProductId());
						inner_product_deets.put("Product_Name", trendingProductList.get(i).getrecos());
						inner_product_deets.put("Product_Sector", trendingProductList.get(i).getlovValue());
						inner_product_deets.put("Bought", trendingProductList.get(i).getboughtValue());
						inner_product_deets.put("Watchlist", trendingProductList.get(i).getwatchlistValue());
						inner_product_deets.put("CMOTS_COCODE", trendingProductList.get(i).getISINValue());
						inner_product_deets.put("ISIN", trendingProductList.get(i).getEXCHANGEEValue());
						inner_product_deets.put("EXCHANGE", trendingProductList.get(i).getCMOTS_COCODEValue());
						inner_product_deets.put("ODINCODE", trendingProductList.get(i).getODINCODEValue());
						
						//  Nate 1
						
						StringBuilder sb = new StringBuilder("{");
						for (Map.Entry<String, Object> entry : inner_product_deets.entrySet()) {
				            String key = entry.getKey();
				            Object value = entry.getValue();
				            sb.append("\"").append(key).append("\":\"");
				            if (value != null) {
				                sb.append(value.toString());
				            }
				            sb.append("\",");
				        }
				        sb.deleteCharAt(sb.length() - 1); // remove the last comma
				        sb.append("}");
				        output.append(sb);
				        output.append(",");
					}
					response.put("Product_List", product_list_1);
					return output.toString();
				}
				public Map<String, Object> getLastOrderProductList(String customer_key) {
					Map<String, Object> response = new HashMap<>();
					// check if reco count greater the limit  
//					if (Integer.parseInt(recoCount)<reco_cnt) {
//						String inx="Given Reco_Count limit is more than allowed set of recommendations of "+recoCount+" recos; Please Connect the CS Team in regards to the Upper Limit default value";
//						Map<String, Object> err_response = new HashMap<String, Object>();
//						err_response.put("code",127);
//					    err_response.put("desc",inx);
//					    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
//						System.out.println(Instant.now() + " | ERROR | " + "Reco_Count More than the specified Limit  ");	
//					    return err_response;
//					}
					List<String> product_list_1 = new ArrayList<String>();
					List<LastOrderProducts> last_order_product_list = lastOrderProductRepo.getLastOrderProductList(customer_key);
					for (int i=0;i<last_order_product_list.size() ;i++) {
						product_list_1.add(last_order_product_list.get(i).getproductId());
					}
					response.put("Products_Purchased", product_list_1);
					return response;
				}
				public String getContextualAnonProductList(String context_key,String context_value,Integer story_id, Integer reco_cnt) {
					Map<String, Object> response = new HashMap<>();
					// check if reco count greater the limit  
					if (Integer.parseInt(recoCount)<reco_cnt) {
						String inx="Given Reco_Count limit is more than allowed set of recommendations of "+recoCount+" recos; Please Connect the CS Team in regards to the Upper Limit default value";
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",127);
					    err_response.put("desc",inx);
					    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						System.out.println(Instant.now() + " | ERROR | " + "Reco_Count More than the specified Limit  ");	
					    return "NA";
					}
					List<String[]> product_list_1 = new ArrayList<String[]>();
					/*
					 * String[] options = {"no View","Buy","Sell","Hold"};
					 * Random random1 = new Random();
					 * int index = random1.nextInt(options.length);
					 * // generate a random index within the range of the array
					 * String randomOption = options[index];
					 */
					//String[] categories = {"Banking","FMCG","IT","Petrochemical"};
					//Random random1 = new Random();
					
					// Declaring the String to return 
					StringBuilder output = new StringBuilder("");
					List<ContextKeyProducts> context_key_product_list = contxtkeyProdrepo.getContextProductList(context_value,story_id);
					for (int i=0;i<context_key_product_list.size() && i<reco_cnt ;i++) {
						Map<String, Object> inner_product_deets = new HashMap<>();
//						inner_product_deets.put("Product_Id", context_key_product_list.get(i).getProductId());
//						inner_product_deets.put("Product_Name", context_key_product_list.get(i).getrecos());
//						inner_product_deets.put("Product_Sector", context_key_product_list.get(i).getlovValue());
						
						//  Nate 1
						inner_product_deets.put("Product_Id", context_key_product_list.get(i).getProductId());
						inner_product_deets.put("Product_Name", context_key_product_list.get(i).getrecos());
						inner_product_deets.put("Product_Sector", context_key_product_list.get(i).getlovValue());
						inner_product_deets.put("Bought", context_key_product_list.get(i).getboughtValue());
						inner_product_deets.put("Watchlist", context_key_product_list.get(i).getwatchlistValue());
						inner_product_deets.put("CMOTS_COCODE", context_key_product_list.get(i).getISINValue());
						inner_product_deets.put("ISIN", context_key_product_list.get(i).getEXCHANGEEValue());
						inner_product_deets.put("EXCHANGE", context_key_product_list.get(i).getCMOTS_COCODEValue());
						inner_product_deets.put("ODINCODE", context_key_product_list.get(i).getODINCODEValue());
						
						
						StringBuilder sb = new StringBuilder("{");
						for (Map.Entry<String, Object> entry : inner_product_deets.entrySet()) {
				            String key = entry.getKey();
				            Object value = entry.getValue();
				            sb.append("\"").append(key).append("\":\"");
				            if (value != null) {
				                sb.append(value.toString());
				            }
				            sb.append("\",");
				        }
				        sb.deleteCharAt(sb.length() - 1); // remove the last comma
				        sb.append("}");
				        output.append(sb);
				        output.append(",");
				        System.out.println("Product String:="+sb);
				        
						//int index = random1.nextInt(categories.length);
						//String randomOption = categories[index];
						//String[] product_lmk ={"",""};
						//System.out.println("context_key_product_list.get(i).getrecos()="+context_key_product_list.get(i).getrecos());
						//product_lmk[0]=context_key_product_list.get(i).getrecos();
						//product_lmk[1]=randomOption;
						//product_list_1.add(product_lmk);
					}
					response.put("Product_List", product_list_1);
					System.out.println("Anon Service Response="+response);
					return output.toString();
				}
				
				// Get Landing Page Recos
				public Map<String, Object> getLandingPageFunction(String bitly_code) {
					Map<String, Object> response = new HashMap<>();
					// Get Product Id List
					try {
						//response=file.getLandingPageList(bitly_code);
						List<LandingPageRecos> landing_recos = landingPagerecos.getLandingPageRecoList(bitly_code);
						String mobile ="";
						String microsite_url ="";
						String full_name ="";
						String email ="";
						String pn_message ="";
						mobile=landing_recos.get(0).getmobile();
						microsite_url=landing_recos.get(0).getmicrosite_url();
						full_name=landing_recos.get(0).getfull_name();
						pn_message=landing_recos.get(0).getpn_message();
						String[] list_1=pn_message.split(",");
						List<Map<String,Object>> item_list = new ArrayList<Map<String,Object>>();
						for (String i: list_1) {
							Map<String,Object> inner_map = new HashMap();
							inner_map.put("Product_Variant_Id",i);
							item_list.add(inner_map);
						}
						response.put("Full_Name",full_name );
						response.put("Email",email );
						response.put("Customer_Key",mobile );
						response.put("Customer_Key",mobile );
						response.put("items", item_list);
						response.put("Bitly_code", bitly_code);
					}
					catch(Exception ex) {
						System.out.println(ex);
						response.put("Error", "Bitly_Code has no Recommendations Present");
						response.put("Bitly_code", bitly_code);
						return response;
					}
					return response;
				}
				
				
				public Map<String, Object> getServiceFunction(List<String> loyalty_ids) {
					Map<String, Object> response = new HashMap<>();
					// Create the Map Product Id List
//				    {
//				        "identity": "468977215",  
//				        "type": "profile",
//				        "profileData": {
//				            "Recos1":{"$add":["shoes2"]},
//				            "Recos2":{"$add":["shoes3"]},
//				            "Recos3":{"$add":["shoes4"]}
//				        }
//				      }
					List<Map<String, Object>> final_list = new ArrayList<Map<String, Object>>();
					try {
						for(String i:loyalty_ids) {
							Map<String, Object> map_inner = new HashMap<>();
							map_inner.put("identity", i);
							map_inner.put("type", "profile");
							Map<String, Object> map_inner_2 = new HashMap<>();
							List<GateWayRecos> base_recos=gatewayRepo.getGateWayProductList(i, "UC001.Hybridizer.Reorder");
							String reco1= (String)base_recos.get(0).getGateWayRecos();
							String[] recos = reco1.split(",");
							Integer k=1;
							Map<String, Object> map_inner_3 = new HashMap<>();
							for(String j:recos) {
								System.out.println("Reco="+j);
								
								Map<String, Object> map_inner_4 = new HashMap<>();
								List<String> product_list = new ArrayList<String>();
								product_list.add(j);
								map_inner_4.put("$add",product_list);
								map_inner_3.put("Recos"+k, map_inner_4);
								k++;
							}
							map_inner.put("profileData", map_inner_3);
							System.out.println("Loyalty_Map="+map_inner);
							final_list.add(map_inner);
						}
					}
					catch(Exception ex) {
						System.out.println(ex);
					}
					response.put("d", final_list);
					return response;
				}
				
				public Map<String, Object> getCustomerLastMessageSent(Map<String, Object> request) {
					Map<String, Object> response = new HashMap<>();
					// Get Product Id List
					try {
						try {
							String cust_key = (String)request.get("Customer_Key");
							try {
								List<LastMessageSent> msg_list= lastmsgRepo.getLastMessageDetails(cust_key);
								String last_msg = msg_list.get(0).getMessage().replace("\"", "");  //getMessage
								response.put("Last Message Sent", last_msg);
							}catch(Exception ex) {
								response.put("Error","Request Customer_Key Does not have any last message");
								return response;
							}
						}catch(Exception ex) {
							response.put("Error","Request Body Does not have Customer_Key");
							return response;
						}
					}
					catch(Exception ex) {
						System.out.println(ex);
					}
					return response;
				}
				
				// Get Clevertap JSON
				
				public Map<String, Object> getJsonRequestOutputFunction(Map<String, Object> loyalty_map) {
					Map<String, Object> response = new HashMap<>();
					List<Map<String, Object>> inner_list = new ArrayList<Map<String, Object>>();
					//loyalty_map
					for (Map.Entry<String, Object> entry : loyalty_map.entrySet()) {
						Map<String, Object> inner_response = new HashMap<>();
						String loyaty_id=entry.getKey();
						inner_response.put("identity",loyaty_id );
						inner_response.put("type","profile" );
						List<String> product_name_list = new ArrayList<String>();
						product_name_list=file.getCustRecos((String)entry.getValue());
						Integer i=0;
						Map<String, Object> inner_response_3 = new HashMap<>();
						for (String product:product_name_list) {
							List<String> single_product_list = new ArrayList<String>();
							single_product_list.add(product);
							Map<String, Object> inner_response_2 = new HashMap<>();
							inner_response_2.put("$add",single_product_list );
							inner_response_3.put("Recos"+i, product);
							i++;
						}
					    inner_response.put("profileData",inner_response_3);
					    inner_list.add(inner_response);
					}
					response.put("d", inner_list);
					return response;
				}
				public String getHashedValue(String input) {
					try   
					{
						MessageDigest msgDst = MessageDigest.getInstance("MD5");
						byte[] msgArr = msgDst.digest(input.getBytes());
						BigInteger bi = new BigInteger(1, msgArr);
						String hshtxt = bi.toString(16);
						while (hshtxt.length() < 32){
							hshtxt = "0" + hshtxt;
						}
						return hshtxt;
						}
					catch (NoSuchAlgorithmException abc){
						return "";
						}
				}
				
				@Async("taskExecutor")
			    public CompletableFuture<Map<String, Object>> callSecondApi(Map<String, Object> response_for_async) {
					String uri="https://in1.api.clevertap.com/1/upload";
					RestTemplate restTemplate = new RestTemplate();
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.set("X-CleverTap-Account-Id","RZ5-R76-WK6Z");
					headers.set("X-CleverTap-Passcode","EVO-IMY-YLUL");
					HttpEntity<Map<String, Object>> request_l = new HttpEntity<Map<String, Object>>(response_for_async,headers);
					ResponseEntity<String> response_http = null;
					response_http=restTemplate.exchange(uri, HttpMethod.POST,request_l,String.class);
					Map<String, Object> map_response_http = new HashMap<>();
					map_response_http.put("HTTPREsponse", response_http);
					System.out.println("#######################################################################################");
					System.out.println("#########################################INSIDE INTERNAL QUEUE##############################################");
					System.out.println("response_http"+response_http);
					System.out.println("map_response_http"+map_response_http);
					return CompletableFuture.completedFuture(map_response_http);
				}
}
