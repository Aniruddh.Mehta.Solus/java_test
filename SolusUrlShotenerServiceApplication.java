package com.solus.solus_url_shotener_service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.solus.solus_url_shotener_service.controller.FileReaderUtility;

@SpringBootApplication
public class SolusUrlShotenerServiceApplication {

	public static String config_filename="";
	public static String event_mapping_filename="";
	public static String static_url_file="";
	public static String url_code_mapping_filename="";
	public static String url_test_data_filename="";
	public static void main(String[] args)  throws IOException {
		config_filename=args[0];
		event_mapping_filename=args[1];
		url_code_mapping_filename=args[2];
		static_url_file=args[3];
		url_test_data_filename=args[4];
		try {
		SpringApplication.run(SolusUrlShotenerServiceApplication.class, args);
			try (InputStream input = new FileInputStream(config_filename)) {
	            Properties prop = new Properties();
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        }		
		}
		catch(Exception e) {
			System.out.println(Instant.now()+" | ERROR | Failed to start Microservice "+e);
		}
		try  {	           
			FileReaderUtility file = new FileReaderUtility();
            file.loadData(event_mapping_filename);
            file.loadTestData(url_test_data_filename);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
	}

}
