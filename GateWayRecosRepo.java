package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.Solus_Microservice.reco_API.entity.GateWayRecos;

@Repository
public interface GateWayRecosRepo extends JpaRepository<GateWayRecos,String>{
	@Query(value="select Recos from  RankedPickList_Stories a ,CDM_Customer_Master b,RankedPickList_Stories_Master c where a.Story_Id=c.Story_Id and a.Customer_Id=b.Customer_Id and Customer_State=:loyaltyId and UseCaseKey=:usecaseKey", nativeQuery=true)
	public List<GateWayRecos>getGateWayProductList(@Param("loyaltyId") String Loyalty_Id,@Param("usecaseKey") String UseCaseKey);
}
