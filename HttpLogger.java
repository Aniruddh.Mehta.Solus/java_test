package com.Solus_Microservice.reco_API.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

import com.Solus_Microservice.reco_API.RecoApiApplication;

@Component
public class HttpLogger  extends AbstractRequestLoggingFilter {
	private String remoteHost;
	private int remotePort;
	private Instant currentTimestamp;
	private String logFilePath = RecoApiApplication.logFilePath;
	private String filename = "Request_Logs_" 
			+ (new SimpleDateFormat("yyyy_MM_dd").format(new Date()))
			+ ".txt";
	private String logFilename = "Test_Logs_" 
			+ (new SimpleDateFormat("yyyy_MM_dd").format(new Date()))
			+ ".txt";
	private String time_logs = "API_TIME_LOG_" 
			+ (new SimpleDateFormat("yyyy_MM_dd").format(new Date()))
			+ ".txt";
		
	public String getRemoteHost() {
		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	public int getRemotePort() {
		return remotePort;
	}

	public void setRemotePort(int remotePort) {
		this.remotePort = remotePort;
	}

	public Instant getCurrentTimestamp() {
		return currentTimestamp;
	}

	public void setCurrentTimestamp(Instant currentTimestamp) {
		this.currentTimestamp = currentTimestamp;
	}

	@Override
	public void beforeRequest(HttpServletRequest request, String message) {
		setRemoteHost(request.getRemoteHost());
		setRemotePort(request.getRemotePort());
		setCurrentTimestamp(Instant.now());
	}
	
	@Override
	public void afterRequest(HttpServletRequest request, String message) {
			System.out.print("");
	}
	
	//Method for writing request log file
	public void logWriter(String content) {
		try {
		    final Path path = Paths.get(logFilePath,filename);
		    Path pathParent = path.getParent();
		    if(!Files.exists(pathParent)) {
		    	Files.createDirectories(pathParent);
		    }
		    Files.write(path, Arrays.asList(content), StandardCharsets.UTF_8,
		        Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		} catch (final IOException ioe) {
		    // Add your own exception handling...
			System.out.println(ioe);
		}
	}
	
	//Method for writing logs in testing mode
	public void testLogWriter(String content) {
		try {
		    final Path path = Paths.get(logFilePath,logFilename);
		    Path pathParent = path.getParent();
		    if(!Files.exists(pathParent)) {
		    	Files.createDirectories(pathParent);
		    }
		    Files.write(path, Arrays.asList(content), StandardCharsets.UTF_8,
		        Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		} catch (final IOException ioe) {
			    // Add your own exception handling...
				System.out.println(ioe);
		}
	}
	public void TimeLogWriter(String content) {
		try {
		    final Path path = Paths.get(logFilePath,time_logs);
		    Path pathParent = path.getParent();
		    if(!Files.exists(pathParent)) {
		    	Files.createDirectories(pathParent);
		    }
		    Files.write(path, Arrays.asList(content), StandardCharsets.UTF_8,
		        Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		} catch (final IOException ioe) {
			    // Add your own exception handling...
				System.out.println(ioe);
		}
	}
}
