package com.Solus_Microservice.reco_API.controller;

import java.net.URI;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.Solus_Microservice.reco_API.RecoApiApplication;
import com.Solus_Microservice.reco_API.service.ApiService;
import com.Solus_Microservice.reco_API.service.HttpLogger;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class ApiController {
	@Autowired
	private ApiService service;
	
	@Autowired
	private HttpLogger httpReq = new HttpLogger();
	private String logLevel = RecoApiApplication.logLevel;
	
	// ##############################################################################################################################
	// Hybridizer Recos 
	@CrossOrigin
	@PostMapping("/solus_hybridizer_recos")
	public Map<String, Object> getProductGroupRecos(@RequestBody Map<String, Object> request) {  //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);
		System.out.println("---------------Endpoint:=/solus_hybridizer_recos---------------------");
		System.out.println(request.get("Customer_email"));
		Object temp_user_email = request.get("Customer_email");
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/solus_hybridizer_recos"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		if(!request.get("Customer_email").toString().equals("")) {
		if(Pattern.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$", request.get("Customer_email").toString())==false) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",121);
            err_response.put("desc","Invalid Email_Ids"); 
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format ");	
            return err_response;
		}
	}
		System.out.println("Verified Email");
		Map<String, Object> response = new HashMap<>();
    	Map<String, Object> responseProductGroup = new HashMap<>();
    	List<Map> check_list = new ArrayList<Map>();
    	try {
    		if(request.get("Customer_email") == null || request.get("Customer_email").equals("")) {
				//For null user
				request.put("Customer_email","-100");
				response = service.getHybridizerReco(request);
				check_list=(List<Map>)response.get("items");
				if (check_list.size()==0) {
					System.out.println("Customer is Null");
					Map<String, Object> err_response_null_cust = new HashMap<String, Object>();
					err_response_null_cust.put("error_code",501);
		            err_response_null_cust.put("desc","There is no Customer Email Attached to the Request"); 
		            err_response_null_cust.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
					return err_response_null_cust;
				}
			}
			if(request.get("Customer_email") != null || !(request.get("Customer_email").equals(""))) {
				//For non - null user
	    		response=service.getHybridizerReco(request);
	    		check_list=(List<Map>)response.get("items");
				if (check_list.size()==0) {
					System.out.println("Item List is Null");
					Map<String, Object> err_response_null_recos = new HashMap<String, Object>();
					err_response_null_recos.put("error_code",502);
		            err_response_null_recos.put("desc","There is no Customer Email Attached to the Request"); 
		            err_response_null_recos.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
					return err_response_null_recos;
				}
			}
			if(response.get("Customer_email").equals("-100")) {
				request.put("Customer_email",temp_user_email);
				response.put("Customer_email",temp_user_email);
			}
			try {
				ObjectMapper mapper = new ObjectMapper();
				String requestJson = mapper.writeValueAsString(request);
				String responseJson = mapper.writeValueAsString(response);
				
			}
			catch(Exception ex) {
				System.out.println(ex);
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + ex);
			}
    		
    	}catch(Exception e) {
			System.out.println(e);
			try {
				ObjectMapper mapper = new ObjectMapper();
				String requestJson = mapper.writeValueAsString(request);
				String responseJson = mapper.writeValueAsString(response);
			}
			catch(Exception ex) {
				System.out.println(ex);
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + ex);				
			}
			if(logLevel.equals("TEST"))
				System.out.println(Instant.now() + " | ERROR | " + e);		
		}
    	/*
    	 * If customer is valid then give top n recommendation from product group followed by  hybridizer reco
    	 * If Customer is null then give regular hybridozer recos 
    	 * process response only to add product to the map 
    	 * 
    	 */
    	if (response.get("items")==null) {
			request.put("Customer_email", "-100");
			response = service.getHybridizerReco(request);
			response.put("Customer_email",request.get("Customer_email"));
			if(logLevel.equals("TEST"))
				System.out.println(Instant.now() + " | WARNING | Null Response returned. Sent top trending products as recos");
		}
    	
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/solus_hybridizer_recos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
		return response;
    	
	}
	
	// ##############################################################################################################################
		// Hybridizer Recos 
		@CrossOrigin
		@PostMapping("/solus_landingpage_recos")
		public Map<String, Object> getLandingPageRecos(@RequestBody Map<String, Object> request) {  //DONE
			Instant start_time = httpReq.getCurrentTimestamp();
			Timestamp start_timestamp = Timestamp.from(start_time);
			// ####################################
			// Intentional Addition of time lag
//			try {
//			Thread.sleep(2000);
//			}catch(Exception e) {
//				System.out.println(e);
//			}
			//#####################################
			System.out.println("---------------Endpoint:=/solus_hybridizer_recos---------------------");
			Map<String, Object> response = new HashMap<>();
	    	try {
	    		String bitly_code=(String)request.get("Bitly_code");
	    		System.out.println(bitly_code);
	    		response=service.getLandingPageFunction(bitly_code);
	    	}catch(Exception e) {
				System.out.println(e);
				response.put("Error", "Invalid Bitly Code ");
				try {
					ObjectMapper mapper = new ObjectMapper();
					String requestJson = mapper.writeValueAsString(request);
					String responseJson = mapper.writeValueAsString(response);
				}
				catch(Exception ex) {
					System.out.println(ex);
					if(logLevel.equals("TEST"))
						System.out.println(Instant.now() + " | ERROR | " + ex);				
				}
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + e);		
			}
	    	
	    	
	    	Instant end_time = Instant.now();
			Timestamp end_timestamp = Timestamp.from(end_time);
	    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
			String requestJson="";
			String responseJson="";
			try {
				ObjectMapper mapper = new ObjectMapper();
				requestJson = mapper.writeValueAsString(request);
				responseJson = mapper.writeValueAsString(response);
			}
			catch(Exception ex) {
				System.out.println(ex);				
			}
			
			// Get the end time and convert it to a Timestamp
			
			// Calculate the time difference in milliseconds
			long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

			System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

			httpReq.TimeLogWriter(hashCol+"|"+"/solus_landingpage_recos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
			String bitly_code=(String)request.get("Bitly_code");
			response.put("Bitly_code", bitly_code);
			return response;
	    	
		}
	
	// #######################################################################################################################
	// Genome Recos
	
	@CrossOrigin
	@PostMapping("/solus_genome_recos")
	public Map<String, Object> getGenomeRecos(@RequestBody Map<String, Object> request) {  //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);
		Object temp_user_email = request.get("Customer_email");
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/solus_genome_recos"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		if(!request.get("Customer_email").toString().equals("")) {
		if(Pattern.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$", request.get("Customer_email").toString())==false) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",121);
            err_response.put("desc","Invalid Email_Ids"); 
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            try {
				ObjectMapper mapper = new ObjectMapper();
				String requestJson = mapper.writeValueAsString(request);
				String responseJson = mapper.writeValueAsString(err_response);
			}
			catch(Exception ex) {
				System.out.println(ex);
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + ex);				
			}
			if(logLevel.equals("TEST"))
				System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format ");	
            return err_response;
		}
	}
		Map<String, Object> response = new HashMap<>();
		Map<String, Object> responseProductGroup = new HashMap<>();
    	try {
    		if(request.get("Customer_email") == null || request.get("Customer_email").equals("")) {
				//For null user
				request.put("Customer_email","-100");
				response = service.getGenomeReco(request);
			}
			if(request.get("Customer_email") != null || !(request.get("Customer_email").equals(""))) {
				//For non - null user
	    		response=service.getGenomeReco(request);
			}
			System.out.println("=========hibridizer response=========="+response);
			if(response.get("Customer_email").equals("-100")) {
				request.put("Customer_email",temp_user_email);
				response.put("Customer_email",temp_user_email);
			}
			try {
				ObjectMapper mapper = new ObjectMapper();
				String requestJson = mapper.writeValueAsString(request);
				String responseJson = mapper.writeValueAsString(response);
				
			}
			catch(Exception ex) {
				System.out.println(ex);
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + ex);
			}
    		
    	}catch(Exception e) {
			System.out.println(e);
			try {
				ObjectMapper mapper = new ObjectMapper();
				String requestJson = mapper.writeValueAsString(request);
				String responseJson = mapper.writeValueAsString(response);
			}
			catch(Exception ex) {
				System.out.println(ex);
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + ex);				
			}
			if(logLevel.equals("TEST"))
				System.out.println(Instant.now() + " | ERROR | " + e);		
		}
    	/*
    	 * If customer is valid then give top n recommendation from product group followed by  hybridizer reco
    	 * If Customer is null then give regular hybridozer recos 
    	 * process response only to add product to the map 
    	 * 
    	 */
    	if (response.get("items")==null) {
			request.put("Customer_email", "-100");
			response = service.getGenomeReco(request);
			response.put("Customer_email",request.get("Customer_email"));
			if(logLevel.equals("TEST"))
				System.out.println(Instant.now() + " | WARNING | Null Response returned. Sent top trending products as recos");
		}
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/solus_genome_recos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
    
		return response;
	}
	// #######################################################################################################################
	// IBCF Recos
	@CrossOrigin
	@PostMapping("/solus_ibcf_recos")
	public Map<String, Object> getibcfRecos(@RequestBody Map<String, Object> request) { //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);
		Object temp_variant_id = request.get("Variant_Id");
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/solus_ibcf_recos"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		if(!request.get("Variant_Id").toString().equals("")) {
			if(Pattern.matches("^s*[a-zA-Z0-9-_]+s*$", request.get("Variant_Id").toString())==false) {
				Map<String, Object> err_response = new HashMap<String, Object>();
				err_response.put("code",121);
	            err_response.put("desc","Invalid variant_id"); 
	            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
	            try {
					ObjectMapper mapper = new ObjectMapper();
					String requestJson = mapper.writeValueAsString(request);
					String responseJson = mapper.writeValueAsString(err_response);
				}
				catch(Exception ex) {
					System.out.println(ex);
					if(logLevel.equals("TEST"))
						System.out.println(Instant.now() + " | ERROR | " + ex);				
				}
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + "Invalid Variant_Id: incorrect format ");	
	            return err_response;
			}	
		}
		Map<String, Object> response = new HashMap<>();
		Map<String, Object> responseProductGroup = new HashMap<>();
    	try {
    		if(request.get("Variant_Id") == null || request.get("Variant_Id").equals("")) {
				//For null user
				request.put("Variant_Id","-100");
				response = service.getIBCFReco(request);
			}
			if(request.get("Variant_Id") != null || !(request.get("Variant_Id").equals(""))) {
				//For non - null user
	    		response=service.getIBCFReco(request);
			}
			System.out.println("=========hibridizer response=========="+response);
			if(response.get("Variant_Id").equals("-100")) {
				request.put("Variant_Id",temp_variant_id);
				response.put("Variant_Id",temp_variant_id);
			}
			try {
				ObjectMapper mapper = new ObjectMapper();
				String requestJson = mapper.writeValueAsString(request);
				String responseJson = mapper.writeValueAsString(response);
				
			}
			catch(Exception ex) {
				System.out.println(ex);
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + ex);
			}
    		
    	}catch(Exception e) {
			System.out.println(e);
			try {
				ObjectMapper mapper = new ObjectMapper();
				String requestJson = mapper.writeValueAsString(request);
				String responseJson = mapper.writeValueAsString(response);
			}
			catch(Exception ex) {
				System.out.println(ex);
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + ex);				
			}
			if(logLevel.equals("TEST"))
				System.out.println(Instant.now() + " | ERROR | " + e);		
		}
    	/*
    	 * If customer is valid then give top n recommendation from product group followed by  hybridizer reco
    	 * If Customer is null then give regular hybridozer recos 
    	 * process response only to add product to the map 
    	 * 
    	 */
    	if (response.get("items")==null) {
			request.put("Variant_Id", "-100");
			response = service.getIBCFReco(request);
			response.put("Variant_Id",request.get("Variant_Id"));
			if(logLevel.equals("TEST"))
				System.out.println(Instant.now() + " | WARNING | Null Response returned. Sent top trending products as recos");
		}
    	
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();
		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);
		httpReq.TimeLogWriter(hashCol+"|"+"/solus_ibcf_recos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
		return response;
	}	
	// #######################################################################################################################
	// Local Text Delivery Report
	@PostMapping("/api/solus/callback_localtext_delrep")
	public Map<String, Object> callback_localtext_delrep(@RequestBody Map<String, Object> inputData) {
		
		Map<String, Object> response = new HashMap<>();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(inputData);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/api/solus/callback_localtext_delrep"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(inputData);
			System.out.println("##################### Checking for Integer Value ######################");
			String targetId= inputData.get("targetId").toString();
			if(targetId.length()>256) {
				Map<String, Object> err_response = new HashMap<String, Object>();
				err_response.put("code",121);
	            err_response.put("desc","Invalid Length at targetID "); 
	            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + " Length at targetID ");	
	            return err_response;
			}
			System.out.println("##################### Checking for Key Values Map ######################");
			String map_value = inputData.get("key_values").toString();
			map_value = map_value.substring(1, map_value.length()-1);           //remove curly brackets
			String[] keyValuePairs = map_value.split(",");              //split the string to creat key-value pairs
			Map<String,String> redo_map_1 = new HashMap<>();               
			for(String pair : keyValuePairs)                        //iterate over the pairs
			{
				
			    String[] entry = pair.split("=");                   //split the pairs to get key and value 
			    redo_map_1.put(entry[0].trim(), entry[1].trim());          //add them to the hashmap and trim whitespaces
			    if(entry[0].toLowerCase().contains("email")) {
			    	if(Pattern.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$", entry[0])==false) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Invalid Email_Ids at "+entry[0]+"="+entry[1]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
			    	if (entry[1].length()>256) {
			    		Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Invalid length(>256) of Email_Ids at "+entry[0]+"="+entry[1]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
			    	}
			    }else {
					if(entry[1].length()>256 & (Pattern.matches("^s*[a-zA-Z0-9-_]+s*$", entry[1])==false)) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Field Value has special characters and  more than 256 characters at "+entry[0]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
					if(entry[1].length()>256) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Field Value has more than 256 characters at "+entry[0]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
					if(Pattern.matches("^s*[a-zA-Z0-9-._+}]+s*$", entry[1].replaceAll("[\\[\\]\\{\\} ]", ""))==false) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Field Value has special characters at "+entry[0]+"="+entry[1]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
				}
			}
			System.out.println("##################### Checking for Profiles Value ######################");
			String map_value_2 = inputData.get("profiles").toString();
			map_value_2 = map_value_2.substring(13, map_value_2.length()-1);
			String[] keyValuePairs_2 = map_value_2.split(",");
			String[] internal_keyValuePairs_2; 
			for(String pair : keyValuePairs_2) {
				keyValuePairs_2 =pair.split("=");
				// Pattern check for email
				if (keyValuePairs_2[0].toLowerCase().contains("email")) {
					if(Pattern.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$", keyValuePairs_2[1])==false) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Invalid Email_Ids at "+keyValuePairs_2[0]+"="+keyValuePairs_2[1]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
					if (keyValuePairs_2[1].length()>256) {
			    		Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Invalid length(>256) of Email_Ids at "+keyValuePairs_2[0]+"="+keyValuePairs_2[1]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
			    	}
				}else {
					if(keyValuePairs_2[1].length()>256 & (Pattern.matches("^s*[a-zA-Z0-9-_]+s*$", keyValuePairs_2[1])==false)) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Field Value has special characters and  more than 256 characters at "+keyValuePairs_2[0]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
					if(keyValuePairs_2[1].length()>256) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Field Value has more than 256 characters at "+keyValuePairs_2[0]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
					if(Pattern.matches("^s*[a-zA-Z0-9-._+}]+s*$", keyValuePairs_2[1].replaceAll("[\\[\\]\\{\\} ]", ""))==false) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Field Value has special characters at "+keyValuePairs_2[0]+"="+keyValuePairs_2[1]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
				}
			}
		}
		catch(Exception ex) {
			System.out.println(ex);
		}
		
		// ##################################### JSON Verification Ends ################################################
		try {
			Map<String, Object> err_response = new HashMap<String, Object>();
			System.out.println(err_response);
			response = service.putData(inputData,"Callback_localtext_DelRep");
			//Logging request
			try {
				if(logLevel.equals("TEST")) {
					ObjectMapper mapper = new ObjectMapper();
					String requestJson = mapper.writeValueAsString(inputData);
					String responseJson = mapper.writeValueAsString(response);
				}
			}
			catch(Exception ex) {
				System.out.println(ex);
			}
		}
		catch(Exception e) {
			System.out.println(e);
			try {
				if(logLevel.equals("TEST")) {
					ObjectMapper mapper = new ObjectMapper();
					String requestJson = mapper.writeValueAsString(inputData);
					String responseJson = mapper.writeValueAsString(response);
				}
			}
			catch(Exception ex) {
				System.out.println(ex);
			}
			Map<String, Object> err_response = new HashMap<String, Object>();
            err_response.put("code",101);
            err_response.put("desc","Invalid Credentials"); 
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            return err_response;		
		}
    	
		return response;
	}

	// #######################################################################################################################
	// Local Text Delivery Report
	@PostMapping("/api/solus/callback_textlocal_sms_delrep")
	public Map<String, Object> callback_textlocal_sms_delrep(@RequestBody Map<String, Object> inputData) {
		Map<String, Object> response = new HashMap<>();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(inputData);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/api/solus/callback_textlocal_sms_delrep"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(inputData);
			System.out.println("##################### Checking for Integer Value ######################");
			String targetId= inputData.get("targetId").toString();
			if(targetId.length()>256) {
				Map<String, Object> err_response = new HashMap<String, Object>();
				err_response.put("code",121);
	            err_response.put("desc","Invalid Length at targetID "); 
	            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + " Length at targetID ");	
	            return err_response;
			}
			System.out.println("##################### Checking for Key Values Map ######################");
			String map_value = inputData.get("key_values").toString();
			map_value = map_value.substring(1, map_value.length()-1);           //remove curly brackets
			String[] keyValuePairs = map_value.split(",");              //split the string to creat key-value pairs
			Map<String,String> redo_map_1 = new HashMap<>();               
			for(String pair : keyValuePairs)                        //iterate over the pairs
			{
				
			    String[] entry = pair.split("=");                   //split the pairs to get key and value 
			    redo_map_1.put(entry[0].trim(), entry[1].trim());          //add them to the hashmap and trim whitespaces
			    if(entry[0].toLowerCase().contains("email")) {
			    	if(Pattern.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$", entry[0])==false) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Invalid Email_Ids at "+entry[0]+"="+entry[1]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
			    	if (entry[1].length()>256) {
			    		Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Invalid length(>256) of Email_Ids at "+entry[0]+"="+entry[1]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
			    	}
			    }else {
					if(entry[1].length()>256 & (Pattern.matches("^s*[a-zA-Z0-9-_]+s*$", entry[1])==false)) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Field Value has special characters and  more than 256 characters at "+entry[0]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
					if(entry[1].length()>256) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Field Value has more than 256 characters at "+entry[0]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
					if(Pattern.matches("^s*[a-zA-Z0-9-._+}]+s*$", entry[1].replaceAll("[\\[\\]\\{\\} ]", ""))==false) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Field Value has special characters at "+entry[0]+"="+entry[1]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
				}
			}
			System.out.println("##################### Checking for Profiles Value ######################");
			String map_value_2 = inputData.get("profiles").toString();
			map_value_2 = map_value_2.substring(13, map_value_2.length()-1);
			String[] keyValuePairs_2 = map_value_2.split(",");
			String[] internal_keyValuePairs_2; 
			for(String pair : keyValuePairs_2) {
				keyValuePairs_2 =pair.split("=");
				// Pattern check for email
				if (keyValuePairs_2[0].toLowerCase().contains("email")) {
					if(Pattern.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$", keyValuePairs_2[1])==false) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Invalid Email_Ids at "+keyValuePairs_2[0]+"="+keyValuePairs_2[1]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
					if (keyValuePairs_2[1].length()>256) {
			    		Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Invalid length(>256) of Email_Ids at "+keyValuePairs_2[0]+"="+keyValuePairs_2[1]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
			    	}
				}else {
					if(keyValuePairs_2[1].length()>256 & (Pattern.matches("^s*[a-zA-Z0-9-_]+s*$", keyValuePairs_2[1])==false)) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Field Value has special characters and  more than 256 characters at "+keyValuePairs_2[0]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
					if(keyValuePairs_2[1].length()>256) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Field Value has more than 256 characters at "+keyValuePairs_2[0]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
					if(Pattern.matches("^s*[a-zA-Z0-9-._+}]+s*$", keyValuePairs_2[1].replaceAll("[\\[\\]\\{\\} ]", ""))==false) {
						Map<String, Object> err_response = new HashMap<String, Object>();
						err_response.put("code",121);
			            err_response.put("desc","Field Value has special characters at "+keyValuePairs_2[0]+"="+keyValuePairs_2[1]); 
			            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						if(logLevel.equals("TEST"))
							System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format at ");	
			            return err_response;
					}
				}
			}
		}
		catch(Exception ex) {
			System.out.println(ex);
		}
		
		// ##################################### JSON Verification Ends ################################################
		try {
			Map<String, Object> err_response = new HashMap<String, Object>();
			System.out.println(err_response);
			response = service.putData(inputData,"Callback_textlocal_sms_DelRep");
			//Logging request
			try {
				if(logLevel.equals("TEST")) {
					ObjectMapper mapper = new ObjectMapper();
					String requestJson = mapper.writeValueAsString(inputData);
					String responseJson = mapper.writeValueAsString(response);
				}
			}
			catch(Exception ex) {
				System.out.println(ex);
			}
		}
		catch(Exception e) {
			System.out.println(e);
			try {
				if(logLevel.equals("TEST")) {
					ObjectMapper mapper = new ObjectMapper();
					String requestJson = mapper.writeValueAsString(inputData);
					String responseJson = mapper.writeValueAsString(response);
				}
			}
			catch(Exception ex) {
				System.out.println(ex);
			}
			Map<String, Object> err_response = new HashMap<String, Object>();
            err_response.put("code",101);
            err_response.put("desc","Invalid Credentials"); 
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            return err_response;		
		}
		return response;
	}	
	
	// #######################################################################################################################
	// RCore Stories Reco
	@PostMapping("/solus_rcorestory_recos")
	public Map<String, Object> getrcoreStoryRecos(@RequestBody Map<String, Object> request) {  //DONE
		//Object temp_story_id = request.get("Story_Key");
		// connect to db 
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);

		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/solus_rcorestory_recos"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		Map<String, Object> response = new HashMap<>();
		response = service.getRcoreStoryReco(request);
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/solus_rcorestory_recos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
		return response;
	}
	
	// #######################################################################################################################
	// Preferences Reco
	@CrossOrigin
	@PostMapping("/solus_preference_recos")
	public Map<String, Object> getPreferenceRecos(@RequestBody Map<String, Object> request) { //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);

		Object temp_user_email = request.get("Customer_email");	
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/solus_preference_recos"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		if(!request.get("Customer_email").toString().equals("")) {
			if(Pattern.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$", request.get("Customer_email").toString())==false) {
				Map<String, Object> err_response = new HashMap<String, Object>();
				err_response.put("code",121);
	            err_response.put("desc","Invalid Email_Ids"); 
	            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
	            try {
					ObjectMapper mapper = new ObjectMapper();
					String requestJson = mapper.writeValueAsString(request);
					String responseJson = mapper.writeValueAsString(err_response);
				}
				catch(Exception ex) {
					System.out.println(ex);
					if(logLevel.equals("TEST"))
						System.out.println(Instant.now() + " | ERROR | " + ex);				
				}
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format ");	
	            return err_response;
			}
		}
			Map<String, Object> response = new HashMap<>();
			try {
				if(request.get("Customer_email") == null || request.get("Customer_email").equals("")) {
					//For null user
					request.put("Customer_email","-100");
				}
				response = service.getPreferenceReco(request);
				if(response.get("Customer_email").equals("-100")) {
					//For null user
					request.put("Customer_email",temp_user_email);
					response.put("Customer_email",temp_user_email);
				}
			
			//Logging request
				try {
					ObjectMapper mapper = new ObjectMapper();
					String requestJson = mapper.writeValueAsString(request);
					String responseJson = mapper.writeValueAsString(response);
				}
				catch(Exception ex) {
					System.out.println(ex);
					if(logLevel.equals("TEST"))
						System.out.println(Instant.now() + " | ERROR | " + ex);
				}
			}
			catch(Exception e) {
				System.out.println(e);
				try {
					ObjectMapper mapper = new ObjectMapper();
					String requestJson = mapper.writeValueAsString(request);
					String responseJson = mapper.writeValueAsString(response);
				}
				catch(Exception ex) {
					System.out.println(ex);
					if(logLevel.equals("TEST"))
						System.out.println(Instant.now() + " | ERROR | " + ex);				
				}
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + e);		
					
			}

	    	Instant end_time = Instant.now();
			Timestamp end_timestamp = Timestamp.from(end_time);
	    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
			String requestJson="";
			String responseJson="";
			try {
				ObjectMapper mapper = new ObjectMapper();
				requestJson = mapper.writeValueAsString(request);
				responseJson = mapper.writeValueAsString(response);
			}
			catch(Exception ex) {
				System.out.println(ex);				
			}
			
			// Get the end time and convert it to a Timestamp
			
			// Calculate the time difference in milliseconds
			long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

			System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

			httpReq.TimeLogWriter(hashCol+"|"+"/solus_preference_recos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
			return response;
		
	}
	
	// #######################################################################################################################
	// Trending City
	@CrossOrigin
	@PostMapping("/solus_trending_city_recos")
	public Map<String, Object> getTrendingCityRecos(@RequestBody Map<String, Object> request) {  //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);
		Object temp_user_email = request.get("City");	
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/solus_trending_city_recos"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		if(!request.get("City").toString().equals("")) {
			if(Pattern.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$", request.get("City").toString())==false) {
				Map<String, Object> err_response = new HashMap<String, Object>();
				err_response.put("code",121);
	            err_response.put("desc","Invalid Email_Ids"); 
	            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
	            try {
					ObjectMapper mapper = new ObjectMapper();
					String requestJson = mapper.writeValueAsString(request);
					String responseJson = mapper.writeValueAsString(err_response);
				}
				catch(Exception ex) {
					System.out.println(ex);
					if(logLevel.equals("TEST"))
						System.out.println(Instant.now() + " | ERROR | " + ex);				
				}
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format ");	
	            return err_response;
			}
		}
			Map<String, Object> response = new HashMap<>();
	    	Instant end_time = Instant.now();
			Timestamp end_timestamp = Timestamp.from(end_time);
	    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
			String requestJson="";
			String responseJson="";
			try {
				ObjectMapper mapper = new ObjectMapper();
				requestJson = mapper.writeValueAsString(request);
				responseJson = mapper.writeValueAsString(response);
			}
			catch(Exception ex) {
				System.out.println(ex);				
			}
			
			// Get the end time and convert it to a Timestamp
			
			// Calculate the time difference in milliseconds
			long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

			System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

			httpReq.TimeLogWriter(hashCol+"|"+"/solus_trending_city_recos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
			return response;
	}
	// #######################################################################################################################
	// RCORE Group Recos
	// ###################
	// Types of Error
	// -customer not found
	// -event type not found
	// -json format invalid
	// -recommendation not available
	// -no of recos invalid, should not exceed
	// -batch size invalid
	// -event type Invalid , select from this list
	@CrossOrigin
	@PostMapping("/solus_rcorestory_grouprecos")
	public Map<String, Object> getRcoreStoryGroupRecos(@RequestBody Map<String, Object> request) { //DONE
		// Checks for Story , Batch Size , Null Customer 
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);

		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/solus_rcorestory_grouprecos"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		String  storyName = request.get("Story_Name").toString();
		Map<String, Object> response = new HashMap<>();
		System.out.println("Point 1:Before get Story State"+new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").format(new Date()));
		Map<String, Object> story_state=service.getStoryState(storyName);
		System.out.println("Point 2:After get Story State"+new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").format(new Date()));
		try {
			story_state.get("Story_State");
			if (story_state.get("Story_State").toString().equals("true")) {
				response.put("Story_State", "Found");
				// Fetch Customer Array List 
				try {
					List<String> customer_list = new ArrayList<String>();
					customer_list=(List<String>)request.get("Customer_Keys");
					// Get Reco Count 
					Map<String, Object> reco_map = new HashMap<>();
					try {
						Integer reco_count =(Integer)request.get("Reco_Count");
						// Get Map for recos for each Customer
						System.out.println("Point 3:Before getting RCORE Reco Map"+new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").format(new Date()));
						reco_map=service.getRcoreGroupRecoMap(customer_list,reco_count,storyName);
					}catch(Exception ex) {
						Map<String, Object> err_response = new HashMap<>();
						err_response.put("Error", "No Integer Reco Count Found");
						return err_response;
					}
					Integer reco_count =(Integer)request.get("Reco_Count");
					return reco_map;
				}catch(Exception ex) {
					Map<String, Object> err_response = new HashMap<>();
					err_response.put("Error", "No Customer Array List Found");
					return err_response;
				}
			}
			System.out.println(story_state);
		}catch(Exception ex) {
			response.put("Story_State", "Not Found");
			return response;
		}
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/solus_rcorestory_grouprecos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
		return response;
	}
	
	// ##############################################################################################################################
	// Personalized Recos 
	@CrossOrigin
	@PostMapping("/get_personalized_recos")
	public String getPersonalizedRecos(@RequestBody Map<String, Object> request) {  //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);
		System.out.println("---------------Endpoint:=/get_personalized_recos---------------------");
		/*
		 * 
		 */
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/solus_trending_city_recos"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		System.out.println("Checking Customer_Key");
		// Check Customer_Key is not null
		try {
			if(!request.get("Customer_Key").toString().equals("")) {
				System.out.println("Customer_Key Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",121);
            err_response.put("desc","Null Customer_Key;Customer_Key Cannot be a null field; Please Add Valid Keys ");
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Customer_Key Cannot be a null field; Please Add Valid Keys ");	
            return "NA";
		}
		// Check Reco_Type is not null
		try {
			if(!request.get("Reco_Type").toString().equals("")) {
				System.out.println("Reco_Type Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",122);
		    err_response.put("desc","Null Reco_Type; Reco_Type Cannot be a null field; Please Add Valid Keys ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Type Cannot be a null field; Please Add Valid Keys ");	
		    return "NA";
		}
		// Check Reco_Type is not null
		try {
			if(!request.get("Reco_Type").toString().equals("")) {
				System.out.println("Reco Count is Not Null");
			}
		}catch(Exception ex){
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",123);
		    err_response.put("desc","Null Reco_Count;Reco_Count Cannot be a Null Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return "NA";
		}
		
		if((request.get("Reco_Count") instanceof Integer)) {
			System.out.println("Reco Count is integer");
		}else {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",124);
		    err_response.put("desc","Non-Integer Reco_Count;Reco_Count Cannot be a non-Integer Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return "NA";
		}
		// Fetch Values
		Object customer_key_obj = request.get("Customer_Key"); //Customer_Key
		Object reco_type_obj = request.get("Reco_Type"); //Customer_Key
		Object reco_count = request.get("Reco_Count"); //Customer_Key
		// Fetch if Reco_Key is valid
		String reco_type=reco_type_obj.toString();
		System.out.println("reco_type:"+reco_type);
		String reco_key_check = service.checkRecoKey(reco_type);
		System.out.println("reco_key_check:="+reco_key_check);
		// Return error for NA Reco Type
		if((reco_key_check.equals("True"))) {
			System.out.println("Reco Type Exists is integer");
		}else {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",125);
		    err_response.put("desc","Reco_Type Does not Exist; Please Enter a valid Reco_Type");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return "NA";
		}
		Integer reco_key_id = service.checkRecoStoryId(reco_type);
		System.out.println(reco_key_id);
		// Valid Customer Check 
		String customer_key=customer_key_obj.toString();
		Integer cust_key_valid_check = service.checkCustomerKey(customer_key);
		if(cust_key_valid_check==-1) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",126);
		    err_response.put("desc","Given Customer_Key Does not Exist in DB; Please Enter a valid Customer_Key");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return "NA";
		}
		// Return Recommendations for the valid Customers
		Map<String, Object> response = new HashMap<>();
		int reco_cnt= Integer.parseInt(request.get("Reco_Count").toString());
		String response_string="";
		response_string = service.getPersonalRecos(customer_key,reco_type,reco_cnt);
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/get_personalized_recos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
		String response_string_2=response_string.substring(0, response_string.length() - 1);
		return "["+response_string_2+"]";
	}
	@CrossOrigin
	@PostMapping("/get_personalized_recos_2")
	public Map<String, Object> getPersonalizedRecos_2(@RequestBody Map<String, Object> request) {
		System.out.println("---------------Endpoint:=/get_personalized_recos---------------------");
		/*
		 * 
		 */
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/solus_trending_city_recos"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		System.out.println("Checking Customer_Key");
		// Check Customer_Key is not null
		try {
			if(!request.get("Customer_Key").toString().equals("")) {
				System.out.println("Customer_Key Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",121);
            err_response.put("desc","Null Customer_Key;Customer_Key Cannot be a null field; Please Add Valid Keys ");
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Customer_Key Cannot be a null field; Please Add Valid Keys ");	
            return err_response;
		}
		// Check Reco_Type is not null
		try {
			if(!request.get("Reco_Type").toString().equals("")) {
				System.out.println("Reco_Type Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",122);
		    err_response.put("desc","Null Reco_Type; Reco_Type Cannot be a null field; Please Add Valid Keys ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Type Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		// Check Reco_Type is not null
		try {
			if(!request.get("Reco_Type").toString().equals("")) {
				System.out.println("Reco Count is Not Null");
			}
		}catch(Exception ex){
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",123);
		    err_response.put("desc","Null Reco_Count;Reco_Count Cannot be a Null Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		
		if((request.get("Reco_Count") instanceof Integer)) {
			System.out.println("Reco Count is integer");
		}else {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",124);
		    err_response.put("desc","Non-Integer Reco_Count;Reco_Count Cannot be a non-Integer Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		// Fetch Values
		Object customer_key_obj = request.get("Customer_Key"); //Customer_Key
		Object reco_type_obj = request.get("Reco_Type"); //Customer_Key
		Object reco_count = request.get("Reco_Count"); //Customer_Key
		// Fetch if Reco_Key is valid
		String reco_type=reco_type_obj.toString();
		System.out.println("reco_type:"+reco_type);
		String reco_key_check = service.checkRecoKey(reco_type);
		System.out.println("reco_key_check:="+reco_key_check);
		// Return error for NA Reco Type
		if((reco_key_check.equals("True"))) {
			System.out.println("Reco Type Exists is integer");
		}else {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",125);
		    err_response.put("desc","Reco_Type Does not Exist; Please Enter a valid Reco_Type");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		Integer reco_key_id = service.checkRecoStoryId(reco_type);
		System.out.println(reco_key_id);
		// Valid Customer Check 
		String customer_key=customer_key_obj.toString();
		Integer cust_key_valid_check = service.checkCustomerKey(customer_key);
		if(cust_key_valid_check==-1) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",126);
		    err_response.put("desc","Given Customer_Key Does not Exist in DB; Please Enter a valid Customer_Key");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		// Return Recommendations for the valid Customers
		Map<String, Object> response = new HashMap<>();
		int reco_cnt= Integer.parseInt(request.get("Reco_Count").toString());
		String response_string ="";
		response_string = service.getPersonalRecos(customer_key,reco_type,reco_cnt);
		return response;
	}
	// ##############################################################################################################################
	// Personalized Recos 
	@CrossOrigin
	@PostMapping("/get_personalized_recos_for_batch")
	public Map<String, Object> getPersonalizedRecosBatch(@RequestBody Map<String, Object> request) {  //DONE
		
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);
		System.out.println("---------------Endpoint:=/get_personalized_recos_for_batch---------------------");
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/get_personalized_recos_for_batch"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		System.out.println("Checking Customer_Key List");
		// Check Customer_Keys are not null
		try {
			if(!request.get("Customer_Keys").toString().equals("")) {
				System.out.println("Customer_Keys Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",121);
            err_response.put("desc","Null Customer_Keys;List of Customer_Keys Cannot be null ; Please Add Valid Keys List");
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Customer_Keys Cannot be a null field; Please Add Valid Keys ");	
            return err_response;
		}
		// Check Reco_Type is not null
		try {
			if(!request.get("Reco_Type").toString().equals("")) {
				System.out.println("Reco_Type Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",122);
		    err_response.put("desc","Null Reco_Type; Reco_Type Cannot be a null field; Please Add Valid Keys ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Type Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		// Check Reco_Type is not null
		try {
			if(!request.get("Reco_Type").toString().equals("")) {
				System.out.println("Reco Count is Not Null");
			}
		}catch(Exception ex){
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",123);
		    err_response.put("desc","Null Reco_Count;Reco_Count Cannot be a Null Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		
		if((request.get("Reco_Count") instanceof Integer)) {
			System.out.println("Reco Count is integer");
		}else {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",124);
		    err_response.put("desc","Non-Integer Reco_Count;Reco_Count Cannot be a non-Integer Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		// Fetch Values
		
		Object customer_key_obj = request.get("Customer_Keys"); //Customer_Key
		Object reco_type_obj = request.get("Reco_Type"); //Customer_Key
		Object reco_count = request.get("Reco_Count"); //Customer_Key
		// Fetch if Reco_Key is valid
		String reco_type=reco_type_obj.toString();
		System.out.println("reco_type:"+reco_type);
		String reco_key_check = service.checkRecoKey(reco_type);
		System.out.println("reco_key_check:="+reco_key_check);
		// Return error for NA Reco Type
		if((reco_key_check.equals("True"))) {
			System.out.println("Reco Type Exists is integer");
		}else {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",125);
		    err_response.put("desc","Reco_Type Does not Exist; Please Enter a valid Reco_Type");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		Integer reco_key_id = service.checkRecoStoryId(reco_type);
		System.out.println(reco_key_id);
		// Valid Customer Check
		
		List<String> customer_keys_list=(List<String>)customer_key_obj;
		Integer cust_key_valid_check = service.checkCustomerKeyList(customer_keys_list);
		if(cust_key_valid_check==-1) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",126);
		    err_response.put("desc","Given set of Customer_Keys Dont not Exist in DB; Please Enter valid set of Customer_Keys");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		// Return Recommendations for the valid Customers
		Map<String, Object> response = new HashMap<>();
		int reco_cnt= Integer.parseInt(request.get("Reco_Count").toString());
		response = service.getPersonalRecosBatch(customer_keys_list,reco_type,reco_cnt);
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/get_personalized_recos_for_batch"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
		return response;
		
	}
	// ##############################################################################################################################
	// Personalized Recos 
	@CrossOrigin
	@PostMapping("/get_copurchase_recos_for_product")
	public Map<String, Object> getCopurchaseRecos(@RequestBody Map<String, Object> request) {
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);

		System.out.println("---------------Endpoint:=/get_copurchase_recos_for_a_product---------------------");
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/get_copurchase_recos_for_a_product"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		System.out.println("Checking Product_Key ");
		// Check Product_Key is not null
		try {
			if(!request.get("Product_Key").toString().equals("")) {
				System.out.println("Product_Key Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",128);
            err_response.put("desc","Null Product_Key;Product_Key Cannot be null ; Please Add Valid Keys ");
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Product_Key Cannot be a null field; Please Add Valid Keys ");	
            return err_response;
		}

		try {
			if(!request.get("Reco_Count").toString().equals("")) {
				System.out.println("Reco Count is Not Null");
			}
		}catch(Exception ex){
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",123);
		    err_response.put("desc","Null Reco_Count;Reco_Count Cannot be a Null Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		
		if((request.get("Reco_Count") instanceof Integer)) {
			System.out.println("Reco Count is integer");
		}else {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",124);
		    err_response.put("desc","Non-Integer Reco_Count;Reco_Count Cannot be a non-Integer Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		Object product_key_obj = request.get("Product_Key"); //Customer_Key
		Object reco_count = request.get("Reco_Count"); //Customer_Key
		String reco_type="ibcf";
		System.out.println("reco_type:"+reco_type);
		String product_key =(String)product_key_obj;
		// Check if anon stories has ibcf
		Integer story_id=service.anonStoryCheck(reco_type);
		System.out.println("story_id="+story_id);
		Map<String, Object> response = new HashMap<>();
		int reco_cnt= Integer.parseInt(request.get("Reco_Count").toString());
		response = service.getCopurchaseRecos(product_key,story_id,reco_cnt);
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/get_copurchase_recos_for_product"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
		return response;
	}

	// ##############################################################################################################################
	// Personalized Recos 
	@CrossOrigin
	@PostMapping("/get_copurchase_recos_for_productlist")
	public Map<String, Object> getCopurchaseRecosList(@RequestBody Map<String, Object> request) { //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);
		System.out.println("---------------Endpoint:=/get_copurchase_recos_for_a_product---------------------");
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/get_copurchase_recos_for_a_product"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		System.out.println("Checking Product_Key ");
		// Check Product_Key is not null
		try {
			if(!request.get("Product_Keys").toString().equals("")) {
				System.out.println("Product_Keys Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",128);
            err_response.put("desc","Null Product_Keys;Product_Keys List Cannot be null ; Please Add Valid Keys ");
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Product_Key Cannot be a null field; Please Add Valid Keys ");	
            return err_response;
		}

		try {
			if(!request.get("Reco_Count").toString().equals("")) {
				System.out.println("Reco Count is Not Null");
			}
		}catch(Exception ex){
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",123);
		    err_response.put("desc","Null Reco_Count;Reco_Count Cannot be a Null Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		
		if((request.get("Reco_Count") instanceof Integer)) {
			System.out.println("Reco Count is integer");
		}else {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",124);
		    err_response.put("desc","Non-Integer Reco_Count;Reco_Count Cannot be a non-Integer Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		Object product_key_list = request.get("Product_Keys"); //Customer_Key
		Object reco_count = request.get("Reco_Count"); //Customer_Key
		String reco_type="ibcf";
		System.out.println("reco_type:"+reco_type);
		try {
			List<String> product_keys = (List<String>)product_key_list;
			System.out.println(product_keys.get(0));
		}catch(Exception ex){
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",129);
		    err_response.put("desc","Null Product_Key List;Product_Key List Cannot be a Null Field; Please Add Valid Keys to the List ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Product_Key_List Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		List<String> product_keys_l = (List<String>)product_key_list;
		// Check if anon stories has ibcf
		Integer story_id=service.anonStoryCheck(reco_type);
		System.out.println("story_id="+story_id);
		Map<String, Object> response = new HashMap<>();
		int reco_cnt= Integer.parseInt(request.get("Reco_Count").toString());
		response = service.getCopurchaseRecosList(product_keys_l,story_id,reco_cnt);
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/get_copurchase_recos_for_productlist"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
		return response;
	}


	// ##############################################################################################################################
	// Personalized Recos 
	@CrossOrigin
	@PostMapping("/get_deals_recos_for_productlist")
	public Map<String, Object> getDealsRecosList(@RequestBody Map<String, Object> request) {  //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);
		System.out.println("---------------Endpoint:=/get_copurchase_recos_for_a_product---------------------");
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/get_copurchase_recos_for_a_product"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		System.out.println("Checking Product_Key ");
		// Check Product_Key is not null
		try {
			if(!request.get("Product_Keys").toString().equals("")) {
				System.out.println("Product_Keys Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",128);
            err_response.put("desc","Null Product_Keys;Product_Keys List Cannot be null ; Please Add Valid Keys ");
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Product_Key Cannot be a null field; Please Add Valid Keys ");	
            return err_response;
		}

		try {
			if(!request.get("Reco_Count").toString().equals("")) {
				System.out.println("Reco Count is Not Null");
			}
		}catch(Exception ex){
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",123);
		    err_response.put("desc","Null Reco_Count;Reco_Count Cannot be a Null Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		
		if((request.get("Reco_Count") instanceof Integer)) {
			System.out.println("Reco Count is integer");
		}else {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",124);
		    err_response.put("desc","Non-Integer Reco_Count;Reco_Count Cannot be a non-Integer Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		Object product_key_list = request.get("Product_Keys"); //Customer_Key
		Object reco_count = request.get("Reco_Count"); //Customer_Key
		String reco_type="deals";
		System.out.println("reco_type:"+reco_type);
		try {
			List<String> product_keys = (List<String>)product_key_list;
			System.out.println(product_keys.get(0));
		}catch(Exception ex){
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",129);
		    err_response.put("desc","Null Product_Key List;Product_Key List Cannot be a Null Field; Please Add Valid Keys to the List ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Product_Key_List Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}
		List<String> product_keys_l = (List<String>)product_key_list;
		// Check if anon stories has ibcf
		Integer story_id=service.anonStoryCheck(reco_type);
		System.out.println("story_id="+story_id);
		Map<String, Object> response = new HashMap<>();
		int reco_cnt= Integer.parseInt(request.get("Reco_Count").toString());
		response = service.getDealsRecosList(product_keys_l,story_id,reco_cnt);

    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/get_deals_recos_for_productlist"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
		return response;
	}	

	// ##############################################################################################################################
	// Personalized Recos 
	@CrossOrigin
	@PostMapping("/get_trending_recos")
	public String getTrendingRecos(@RequestBody Map<String, Object> request) { //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);
		
		System.out.println("---------------Endpoint:=/get_copurchase_recos_for_a_product---------------------");
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/get_copurchase_recos_for_a_product"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		try {
			if(!request.get("Reco_Count").toString().equals("")) {
				System.out.println("Reco Count is Not Null");
			}
		}catch(Exception ex){
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",123);
		    err_response.put("desc","Null Reco_Count;Reco_Count Cannot be a Null Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return "NA";
		}
		
		if((request.get("Reco_Count") instanceof Integer)) {
			System.out.println("Reco Count is integer");
		}else {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",124);
		    err_response.put("desc","Non-Integer Reco_Count;Reco_Count Cannot be a non-Integer Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return "NA";
		}
		Object reco_count = request.get("Reco_Count"); //Customer_Key
		String reco_type="trending";
		System.out.println("reco_type:"+reco_type);
		Integer story_id=service.anonStoryCheck(reco_type);
		System.out.println("story_id="+story_id);
		Map<String, Object> response = new HashMap<>();
		int reco_cnt= Integer.parseInt(request.get("Reco_Count").toString());
		String response_string="";
		response_string = service.getTrendingRecosList(story_id,reco_cnt);
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/get_trending_recos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
		String response_string_2 =response_string.substring(0, response_string.length() - 1);
		return "["+response_string_2+"]";
		//return response_string;
	}
	
	// ##############################################################################################################################
	// Personalized Recos 
	@CrossOrigin
	@PostMapping("/get_last_order")
	public Map<String, Object> getLastOrderProducts(@RequestBody Map<String, Object> request) {  //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);
		System.out.println("---------------Endpoint:=/get_copurchase_recos_for_a_product---------------------");
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/get_copurchase_recos_for_a_product"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
//		try {
//			if(!request.get("Reco_Count").toString().equals("")) {
//				System.out.println("Reco Count is Not Null");
//			}
//		}catch(Exception ex){
//			Map<String, Object> err_response = new HashMap<String, Object>();
//			err_response.put("code",123);
//		    err_response.put("desc","Null Reco_Count;Reco_Count Cannot be a Null Field; Please Add Valid Integers ");
//		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
//			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
//		    return err_response;
//		}
		
//		if((request.get("Reco_Count") instanceof Integer)) {
//			System.out.println("Reco Count is integer");
//		}else {
//			Map<String, Object> err_response = new HashMap<String, Object>();
//			err_response.put("code",124);
//		    err_response.put("desc","Non-Integer Reco_Count;Reco_Count Cannot be a non-Integer Field; Please Add Valid Integers ");
//		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
//			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
//		    return err_response;
//		}
		
		try {
			if(!request.get("Customer_Key").toString().equals("")) {
				System.out.println("Customer_Key Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",121);
            err_response.put("desc","Null Customer_Key;Customer_Key Cannot be a null field; Please Add Valid Keys ");
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Customer_Key Cannot be a null field; Please Add Valid Keys ");	
            return err_response;
		}		

		Object customer_key_obj = request.get("Customer_Key"); //Customer_Key
		String customer_key=customer_key_obj.toString();
		Integer cust_key_valid_check = service.checkCustomerKey(customer_key);
		if(cust_key_valid_check==-1) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",126);
		    err_response.put("desc","Given Customer_Key Does not Exist in DB; Please Enter a valid Customer_Key");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
		    return err_response;
		}		
		
//		Object reco_count = request.get("Reco_Count"); //Customer_Key
		String reco_type="trending";
		System.out.println("reco_type:"+reco_type);
		Integer story_id=service.anonStoryCheck(reco_type);
		System.out.println("story_id="+story_id);
		Map<String, Object> response = new HashMap<>();
//		int reco_cnt= Integer.parseInt(request.get("Reco_Count").toString());
		response = service.getLastOrderProductList(customer_key);
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/get_last_order"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
		return response;
	}
	

	// ##############################################################################################################################
	// Personalized Recos 
	@CrossOrigin
	@PostMapping("/get_anonymized_contextual_recos")
	public String getAnonContextProducts(@RequestBody Map<String, Object> request) { //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);

		System.out.println("---------------Endpoint:=/get_anonymized_contextual_recos---------------------");
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/get_copurchase_recos_for_a_product"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		try {
			if(!request.get("Reco_Count").toString().equals("")) {
				System.out.println("Reco Count is Not Null");
			}
		}catch(Exception ex){
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",123);
		    err_response.put("desc","Null Reco_Count;Reco_Count Cannot be a Null Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");
			StringBuilder sb = new StringBuilder("{");
	        for (Map.Entry<String, Object> entry : err_response.entrySet()) {
	            String key = entry.getKey();
	            Object value = entry.getValue();
	            sb.append("\"").append(key).append("\":\"");
	            if (value != null) {
	                sb.append(value.toString());
	            }
	            sb.append("\",");
	        }
	        sb.deleteCharAt(sb.length() - 1); // remove the last comma
	        sb.append("}");
	        return sb.toString();
		    //return err_response;
		}
		
		if((request.get("Reco_Count") instanceof Integer)) {
			System.out.println("Reco Count is integer");
		}else {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",124);
		    err_response.put("desc","Non-Integer Reco_Count;Reco_Count Cannot be a non-Integer Field; Please Add Valid Integers ");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
			StringBuilder sb = new StringBuilder("{");
	        for (Map.Entry<String, Object> entry : err_response.entrySet()) {
	            String key = entry.getKey();
	            Object value = entry.getValue();
	            sb.append("\"").append(key).append("\":\"");
	            if (value != null) {
	                sb.append(value.toString());
	            }
	            sb.append("\",");
	        }
	        sb.deleteCharAt(sb.length() - 1); // remove the last comma
	        sb.append("}");
	        return sb.toString();
			//return err_response;
		}
		
		try {
			if(!request.get("Context_Key").toString().equals("")) {
				System.out.println("Context_Key Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",130);
            err_response.put("desc","Null Context_Key;Context_Key Cannot be a null field; Please Add Valid Keys ");
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Customer_Key Cannot be a null field; Please Add Valid Keys ");	
			StringBuilder sb = new StringBuilder("{");
	        for (Map.Entry<String, Object> entry : err_response.entrySet()) {
	            String key = entry.getKey();
	            Object value = entry.getValue();
	            sb.append("\"").append(key).append("\":\"");
	            if (value != null) {
	                sb.append(value.toString());
	            }
	            sb.append("\",");
	        }
	        sb.deleteCharAt(sb.length() - 1); // remove the last comma
	        sb.append("}");
	        return sb.toString();
			//return err_response;
		}
		
		try {
			if(!request.get("Context_Value").toString().equals("")) {
				System.out.println("Context_Value Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",131);
            err_response.put("desc","Null Context_Value;Context_Value Cannot be a null field; Please Add Valid Keys ");
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Context_Value Cannot be a null field; Please Add Valid Keys ");	
			StringBuilder sb = new StringBuilder("{");
	        for (Map.Entry<String, Object> entry : err_response.entrySet()) {
	            String key = entry.getKey();
	            Object value = entry.getValue();
	            sb.append("\"").append(key).append("\":\"");
	            if (value != null) {
	                sb.append(value.toString());
	            }
	            sb.append("\",");
	        }
	        sb.deleteCharAt(sb.length() - 1); // remove the last comma
	        sb.append("}");
	        return sb.toString();
			//return err_response;
		}
		

		Object context_key_obj = request.get("Context_Key"); //Customer_Key
		Object context_key_value_obj = request.get("Context_Value"); //Customer_Key
		/*
		 * Specific Modification for Axis Bank For Story Based Mapping 
		 * Mapping of Story Name to column name here 
		 * Email=City
		 * Gender=Gender
		 * Tier_Name=AgeBracket
		 * Custom_Tag1=Income Range
		 * Custom_Tag_3=occupation
		 */
				
		String context_key = (String)context_key_obj; 
		String context_value=(String)context_key_value_obj;
		// Axis Modification
		if(context_key.equals("City")) {
			context_key="Email_Consent_LOV_Id";
		}else if(context_key.equals("Gender")) {
			context_key="Gender_LOV_Id";
		}else if(context_key.equals("Age Bracket")) {
			context_key="Tier_Name_LOV_Id";
		}else if(context_key.equals("Income Range")) {
			context_key="Custom_Tag1_LOV_Id";
		}else if(context_key.equals("Occupation")) {
			context_key="Custom_Tag3_LOV_Id";
		}
		// Axis Modification
		Integer context_key_valid_check = service.checkContextKey(context_key);
		if(context_key_valid_check==-1) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",132);
		    err_response.put("desc","Given Context_Key Does not Exist in DB; Please Enter a valid Context_Key");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Reco_Count Cannot be a null field; Please Add Valid Keys ");	
			StringBuilder sb = new StringBuilder("{");
	        for (Map.Entry<String, Object> entry : err_response.entrySet()) {
	            String key = entry.getKey();
	            Object value = entry.getValue();
	            sb.append("\"").append(key).append("\":\"");
	            if (value != null) {
	                sb.append(value.toString());
	            }
	            sb.append("\",");
	        }
	        sb.deleteCharAt(sb.length() - 1); // remove the last comma
	        sb.append("}");
	        return sb.toString();
			//return err_response;
		}
		
		System.out.println("context_key_valid_check:- "+context_key_valid_check);
		Object reco_count = request.get("Reco_Count"); //Customer_Key
		Map<String, Object> response = new HashMap<>();
		String response_String = "";
		int reco_cnt= Integer.parseInt(request.get("Reco_Count").toString());
		response_String = service.getContextualAnonProductList(context_key,context_value ,context_key_valid_check,reco_cnt);
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/get_anonymized_contextual_recos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
		StringBuilder sb = new StringBuilder("{");
        for (Map.Entry<String, Object> entry : response.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            sb.append("\"").append(key).append("\":\"");
            if (value != null) {
                sb.append(value.toString());
            }
            sb.append("\",");
        }
        sb.deleteCharAt(sb.length() - 1); // remove the last comma
        sb.append("}");
        //return sb.toString();
        String response_String_2=response_String.substring(0, response_String.length() - 1);
		return "["+response_String_2+"]";
		//return response_String;
	}

	// ##############################################################################################################################
	// Hybridizer Recos 
	@CrossOrigin
	@PostMapping("/shopify_hybridizer_recos")
	public Map<String, Object> getShopifyProductGroupRecos(@RequestBody Map<String, Object> request) { //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);

		System.out.println("---------------Endpoint:=/shopify_hybridizer_recos---------------------");
		System.out.println(request.get("Customer_email"));
		Object temp_user_email = request.get("Customer_email");
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/solus_hybridizer_recos"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		if(!request.get("Customer_email").toString().equals("")) {
		if(Pattern.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$", request.get("Customer_email").toString())==false) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",121);
            err_response.put("desc","Invalid Email_Ids"); 
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format ");	
            return err_response;
		}
	}
		System.out.println("Verified Email");
		Map<String, Object> response = new HashMap<>();
    	Map<String, Object> responseProductGroup = new HashMap<>();
    	List<Map> check_list = new ArrayList<Map>();
    	try {
    		if(request.get("Customer_email") == null || request.get("Customer_email").equals("")) {
				//For null user
				request.put("Customer_email","-100");
				response = service.getHybridizerReco(request);
				check_list=(List<Map>)response.get("items");
				if (check_list.size()==0) {
					System.out.println("Customer is Null");
					Map<String, Object> err_response_null_cust = new HashMap<String, Object>();
					err_response_null_cust.put("error_code",501);
		            err_response_null_cust.put("desc","There is no Customer Email Attached to the Request"); 
		            err_response_null_cust.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
					return err_response_null_cust;
				}
			}
			if(request.get("Customer_email") != null || !(request.get("Customer_email").equals(""))) {
				//For non - null user
	    		response=service.getHybridizerReco(request);
	    		check_list=(List<Map>)response.get("items");
	    		if (check_list.size()==0) {
					System.out.println("Item List is Null");
					Map<String, Object> err_response_null_recos = new HashMap<String, Object>();
					err_response_null_recos.put("error_code",502);
		            err_response_null_recos.put("desc","There is no Customer Email Attached to the Request"); 
		            err_response_null_recos.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
					return err_response_null_recos;
				}
			}
			if(response.get("Customer_email").equals("-100")) {
				request.put("Customer_email",temp_user_email);
				response.put("Customer_email",temp_user_email);
			}
			try {
				ObjectMapper mapper = new ObjectMapper();
				String requestJson = mapper.writeValueAsString(request);
				String responseJson = mapper.writeValueAsString(response);
				
			}
			catch(Exception ex) {
				System.out.println(ex);
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + ex);
			}
    		
    	}catch(Exception e) {
			System.out.println(e);
			try {
				ObjectMapper mapper = new ObjectMapper();
				String requestJson = mapper.writeValueAsString(request);
				String responseJson = mapper.writeValueAsString(response);
			}
			catch(Exception ex) {
				System.out.println(ex);
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + ex);				
			}
			if(logLevel.equals("TEST"))
				System.out.println(Instant.now() + " | ERROR | " + e);		
		}
    	/*
    	 * If customer is valid then give top n recommendation from product group followed by  hybridizer reco
    	 * If Customer is null then give regular hybridozer recos 
    	 * process response only to add product to the map 
    	 * 
    	 */
    	if (response.get("items")==null) {
			request.put("Customer_email", "-100");
			response = service.getHybridizerReco(request);
			response.put("Customer_email",request.get("Customer_email"));
			if(logLevel.equals("TEST"))
				System.out.println(Instant.now() + " | WARNING | Null Response returned. Sent top trending products as recos");
		}
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/shopify_hybridizer_recos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
    	return response;
    	
	}

	// ##############################################################################################################################
	// Hybridizer Recos 
	@CrossOrigin
	@PostMapping("/shopify_genome_recos")
	public Map<String, Object> getShopifyProductGenomeRecos(@RequestBody Map<String, Object> request) {  //DONE
		Instant start_time = httpReq.getCurrentTimestamp();
		Timestamp start_timestamp = Timestamp.from(start_time);
		
		System.out.println("---------------Endpoint:=/shopify_hybridizer_recos---------------------");
		System.out.println(request.get("Customer_email"));
		Object temp_user_email = request.get("Customer_email");
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			String requestLog = httpReq.getCurrentTimestamp()+"|"+
					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
					"/solus_hybridizer_recos"+"|"+requestJson;
			httpReq.logWriter(requestLog);
		}
		catch(Exception ex) {
			System.out.println(ex);
			if(logLevel.equals("TEST"))
				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
		}
		if(!request.get("Customer_email").toString().equals("")) {
		if(Pattern.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$", request.get("Customer_email").toString())==false) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",121);
            err_response.put("desc","Invalid Email_Ids"); 
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format ");	
            return err_response;
		}
	}
		System.out.println("Verified Email");
		Map<String, Object> response = new HashMap<>();
    	Map<String, Object> responseProductGroup = new HashMap<>();
    	List<Map> check_list = new ArrayList<Map>();
    	try {
    		if(request.get("Customer_email") == null || request.get("Customer_email").equals("")) {
				//For null user
				request.put("Customer_email","-100");
				response = service.getGenomeReco(request);
				check_list=(List<Map>)response.get("items");
				if (check_list.size()==0) {
					System.out.println("Customer is Null");
					Map<String, Object> err_response_null_cust = new HashMap<String, Object>();
					err_response_null_cust.put("error_code",501);
		            err_response_null_cust.put("desc","There is no Customer Email Attached to the Request"); 
		            err_response_null_cust.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
					return err_response_null_cust;
				}
			}
			if(request.get("Customer_email") != null || !(request.get("Customer_email").equals(""))) {
				//For non - null user
	    		response=service.getGenomeReco(request);
	    		check_list=(List<Map>)response.get("items");
	    		if (check_list.size()==0) {
					System.out.println("Item List is Null");
					Map<String, Object> err_response_null_recos = new HashMap<String, Object>();
					err_response_null_recos.put("error_code",502);
		            err_response_null_recos.put("desc","There is no Customer Email Attached to the Request"); 
		            err_response_null_recos.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
					return err_response_null_recos;
				}
			}
			if(response.get("Customer_email").equals("-100")) {
				request.put("Customer_email",temp_user_email);
				response.put("Customer_email",temp_user_email);
			}
			try {
				ObjectMapper mapper = new ObjectMapper();
				String requestJson = mapper.writeValueAsString(request);
				String responseJson = mapper.writeValueAsString(response);
				
			}
			catch(Exception ex) {
				System.out.println(ex);
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + ex);
			}
    		
    	}catch(Exception e) {
			System.out.println(e);
			try {
				ObjectMapper mapper = new ObjectMapper();
				String requestJson = mapper.writeValueAsString(request);
				String responseJson = mapper.writeValueAsString(response);
			}
			catch(Exception ex) {
				System.out.println(ex);
				if(logLevel.equals("TEST"))
					System.out.println(Instant.now() + " | ERROR | " + ex);				
			}
			if(logLevel.equals("TEST"))
				System.out.println(Instant.now() + " | ERROR | " + e);		
		}
    	/*
    	 * If customer is valid then give top n recommendation from product group followed by  hybridizer reco
    	 * If Customer is null then give regular hybridozer recos 
    	 * process response only to add product to the map 
    	 * 
    	 */
    	if (response.get("items")==null) {
			request.put("Customer_email", "-100");
			response = service.getGenomeReco(request);
			response.put("Customer_email",request.get("Customer_email"));
			if(logLevel.equals("TEST"))
				System.out.println(Instant.now() + " | WARNING | Null Response returned. Sent top trending products as recos");
		}
    	Instant end_time = Instant.now();
		Timestamp end_timestamp = Timestamp.from(end_time);
    	String hashCol = service.getHashedValue(Math.random()+start_timestamp.toString()+"::"+end_timestamp.toString());
		String requestJson="";
		String responseJson="";
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestJson = mapper.writeValueAsString(request);
			responseJson = mapper.writeValueAsString(response);
		}
		catch(Exception ex) {
			System.out.println(ex);				
		}
		
		// Get the end time and convert it to a Timestamp
		
		// Calculate the time difference in milliseconds
		long timeDifferenceMillis = end_timestamp.getTime() - start_timestamp.getTime();

		System.out.println("Time difference in milliseconds: " + timeDifferenceMillis);

		httpReq.TimeLogWriter(hashCol+"|"+"/shopify_genome_recos"+"|"+start_timestamp.toString()+"|"+end_timestamp.toString()+"|"+requestJson+"|"+responseJson+"|"+timeDifferenceMillis);
    	return response;
    	
	}

	// ##############################################################################################################################
	// Hybridizer Recos 
//	@CrossOrigin
//	@PostMapping("/solus_genome_recos")
//	public Map<String, Object> getCustkeyInput(@RequestBody Map<String, Object> request) {
//		System.out.println("---------------Endpoint:=/shopify_hybridizer_recos---------------------");
//		System.out.println(request.get("Customer_email"));
//		Object temp_user_email = request.get("Customer_email");
//		try {
//			ObjectMapper mapper = new ObjectMapper();
//			String requestJson = mapper.writeValueAsString(request);
//			String requestLog = httpReq.getCurrentTimestamp()+"|"+
//					httpReq.getRemoteHost()+":"+httpReq.getRemotePort()+"|"+
//					"/solus_hybridizer_recos"+"|"+requestJson;
//			httpReq.logWriter(requestLog);
//		}
//		catch(Exception ex) {
//			System.out.println(ex);
//			if(logLevel.equals("TEST"))
//				httpReq.testLogWriter(Instant.now() + " | ERROR | " + ex);				
//		}
//		if(!request.get("Customer_email").toString().equals("")) {
//		if(Pattern.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$", request.get("Customer_email").toString())==false) {
//			Map<String, Object> err_response = new HashMap<String, Object>();
//			err_response.put("code",121);
//            err_response.put("desc","Invalid Email_Ids"); 
//            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
//			System.out.println(Instant.now() + " | ERROR | " + "Invalid Email_Id: incorrect format ");	
//            return err_response;
//		}
//	}
//		System.out.println("Verified Email");
//		Map<String, Object> response = new HashMap<>();
//		return response;
//	}
	
	@RequestMapping("/")
	@GetMapping("/redirectWithRedirectView")
	public RedirectView redirectWithUsingRedirectView(RedirectAttributes attributes) {
		attributes.addFlashAttribute("flashAttribute", "redirectWithRedirectView");
		attributes.addAttribute("attribute", "redirectWithRedirectView");
		return new RedirectView("redirectedUrl");
	    }
	
	@GetMapping(value = "/redirect")
    public ResponseEntity<Void> redirect(@RequestParam Map<String,String> input){
        System.out.println(input);
        String main_url="http://google.com/?n43zz38&utm_source=ss&utm_medium=SMS_solus&utm_campaign=Lapser_SMS1&utm_desc=Lead_Conversion_((Full_Base))_SMS_New_Lapser_SMS1_SMS_NONE&utm_key=1|Varun%20Beverages,2|Hindi%20Aeronautics,3|Indian%20Bank,4|ITC,6|Adani%20Power%20Ltd,7|TVS%20Motors,8|Commins%20India,9|M&M%20Fin%20Serv,10|Bank%20of%20Baroda,11|Infosys%20Ltd";
        //return ResponseEntity.status(HttpStatus.FOUND).location(URI.create("https://google.com/?n43zz38&utm_source=ss&utm_medium=SMS_solus&utm_campaign=Lapser_SMS1&utm_desc=Lead_Conversion_((Full_Base))_SMS_New_Lapser_SMS1_SMS_NONE&utm_key=1)Varun%20Beverages,2)Hindi%20Aeronautics,3)Indian%20Bank,4)ITC,6)Adani%20Power%20Ltd,7)TVS%20Motors,8)Commins%20India,9)M&M%20Fin%20Serv,10)Bank%20of%20Baroda,11)Infosys%20Ltd")).build();
        return ResponseEntity.status(HttpStatus.FOUND).location(URI.create("https://cartui.solus.ai/axisDynamicView/axisDynamic.html")).build();
    }
	
	@CrossOrigin
	@PostMapping("/get_custName")
	public Map<String, Object> get_custName(@RequestBody Map<String, Object> request) {
		String microURL = (String)request.get("Code");
		System.out.println("Temp API Call"+microURL);
		Map<String, Object> response = new HashMap<>();
		response.put("Name", "John Doe");
		return response;
	}
	
	@CrossOrigin
	@PostMapping("/get_CompDesc")
	public Map<String, Object> get_CompDesc(@RequestBody Map<String, Object> request) {
		Map<String, Object> response = new HashMap<>();
		String Comp_Name = (String)request.get("Comp_Name");
		System.out.println("Temp API Call"+Comp_Name);
		response.put("Stock Price", "211.85");
		response.put("Price Increase", "6.85");
		response.put("Percent Increase", "2.33");
		return response;
	}
	@CrossOrigin
	@PostMapping("/get_ProductPageURL")
	public Map<String, Object> get_ProductPageURL(@RequestBody Map<String, Object> request) {
		Map<String, Object> response = new HashMap<>();
		String Product_Key = (String)request.get("Product_Key");
		System.out.println("Temp API Call"+Product_Key);
		response.put("Product URL", "https://www.google.com/?product_url_sample");
		return response;
	}
	
	@CrossOrigin
	@PostMapping("/fetch_loyalty_ids")
	public Map<String,Object> getSQLDSQuery(@RequestBody Map<String, Object> request){
		Map<String, Object> response = new HashMap<>();
		Map<String, Object> response_1 = new HashMap<>();
		Map<String, Object> loyalty_map = new HashMap<>();
		Map<String, Object> response_l = new HashMap<>();
		List<Map<String, Object>> inner_list = new ArrayList<Map<String, Object>>();
		try {
			List<String> loyalty_id_list = new ArrayList<String>();
			loyalty_id_list=(List<String>)request.get("Loyalty_ids");
			System.out.println("Printing Loyalty Ids");
			for(String i:loyalty_id_list) {
				System.out.println(i);
				System.out.println("Feeding Loyalty Ids to for Fetching recos");
			}
			loyalty_map=service.getServiceFunction(loyalty_id_list);
		}catch(Exception ex) {
			return response;				
		}
		
		response_l=service.getJsonRequestOutputFunction(loyalty_map);
		
		
		String uri="https://in1.api.clevertap.com/1/upload";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("X-CleverTap-Account-Id","RZ5-R76-WK6Z");
		headers.set("X-CleverTap-Passcode","EVO-IMY-YLUL");
		HttpEntity<Map<String, Object>> request_l = new HttpEntity<Map<String, Object>>(response_l,headers);
		ResponseEntity<String> response_http = null;
		response_http=restTemplate.exchange(uri, HttpMethod.POST,request_l,String.class);
		ObjectMapper mapper = new ObjectMapper();
		try {
		String response_k = mapper.writeValueAsString(response_http);
		System.out.println("Final String---------------------"+response_k);
		}catch(Exception ex) {
			System.out.println(ex);
		}
		return response_l;
	}
	
	
	@CrossOrigin
	@PostMapping("/solus_recogateway_ct")
	public Map<String,Object> getRecoGateWay_CT(@RequestBody Map<String, Object> request){
		// Recieve Profiles
		Map<String, Object> response = new HashMap<>();
		Map<String, Object> response_1 = new HashMap<>();
		Map<String, Object> response_for_async = new HashMap<>();
		try {
			List<Map<String, Object>> profile_map = new ArrayList<Map<String, Object>>();
			profile_map=(List<Map<String, Object>>)request.get("profiles");
			// Collect Profile Ids 
			try {
				List<String> loyalty_id_list = new ArrayList<String>();
				for(Map<String, Object> cust_map:profile_map) {
					loyalty_id_list.add((String)cust_map.get("identity"));
				}
				System.out.println("loyalty_id_list"+loyalty_id_list);
				response_1=service.getServiceFunction(loyalty_id_list);
				//response_for_async=service.getJsonRequestOutputFunction(response_1);
				//service.callSecondApi(response_for_async);
				response.put("Status","Request Sent Successfully");
			}
			catch(Exception ex) {
				response.put("Error", "Profiles component in the json Body doesent have list of profiles");
				return response;				
			}
			// ##############################################
			String uri="https://in1.api.clevertap.com/1/upload";
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("X-CleverTap-Account-Id","RZ5-R76-WK6Z");
			headers.set("X-CleverTap-Passcode","EVO-IMY-YLUL");
			HttpEntity<Map<String, Object>> request_l = new HttpEntity<Map<String, Object>>(response_1,headers);
			ResponseEntity<String> response_http = null;
			response_http=restTemplate.exchange(uri, HttpMethod.POST,request_l,String.class);
			Map<String, Object> map_response_http = new HashMap<>();
			map_response_http.put("HTTPREsponse", response_http);
			System.out.println("#######################################################################################");
			System.out.println("#########################################INSIDE INTERNAL QUEUE##############################################");
			System.out.println("response_http"+response_http);
			System.out.println("map_response_http"+map_response_http);
			// ################################################
			return response;
		}
		catch(Exception ex) {
			response.put("Error", "No List of Profiles found in the json Body");
			return response;				
		}	
	}
	
	@CrossOrigin
	@PostMapping("/solus_getLastMessage")
	public String getLastMessage(@RequestBody Map<String, Object> request){
		Map<String, Object> response = new HashMap<>();
		try{
			response=service.getCustomerLastMessageSent(request);
		}catch(Exception ex) {
			response.put("Error", "No List of Profiles found in the json Body");
			StringBuilder sb = new StringBuilder("{");
	        for (Map.Entry<String, Object> entry : response.entrySet()) {
	            String key = entry.getKey();
	            Object value = entry.getValue();
	            sb.append("\"").append(key).append("\":\"");
	            if (value != null) {
	                sb.append(value.toString());
	            }
	            sb.append("\",");
	        }
	        sb.deleteCharAt(sb.length() - 1); // remove the last comma
	        sb.append("}");
	        return sb.toString();
			//return response;				
		}
		//return response;
		StringBuilder sb = new StringBuilder("{");
        for (Map.Entry<String, Object> entry : response.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            sb.append("\"").append(key).append("\":\"");
            if (value != null) {
                sb.append(value.toString());
            }
            sb.append("\",");
        }
        sb.deleteCharAt(sb.length() - 1); // remove the last comma
        sb.append("}");
        return sb.toString();
	}
	
	@CrossOrigin
	@PostMapping("/dummy_stock_values")
	public String getzerRecos(@RequestBody Map<String, Object> request) {
    	Map<String,Object> response = new HashMap<String,Object>();
    	StringBuilder output = new StringBuilder("");
    	if(request.get("Stock_Names")!=null) {
    		try {
    			Object stck_name=request.get("Stock_Names");
        		ObjectMapper mapper = new ObjectMapper();
        		String requestJson = mapper.writeValueAsString(request);
        		requestJson=requestJson.substring(1);
        		requestJson=requestJson.substring(0, requestJson.length() - 1);
        		String[] aryLst = requestJson.split(",");
        		Integer n = aryLst.length;
        		for(int i=0;i<n;i++) {
        			Random random = new Random();
        	    	int randomNumber = random.nextInt(1000) + 10;
        	    	response.put("Stock Price", randomNumber);
        	    	randomNumber = random.nextInt(1000) + 1;
        	    	response.put("Stock Increment Points", randomNumber);
        	    	randomNumber = random.nextInt(100) + 1;
        	    	response.put("Stock Increment Percentage", randomNumber);
        	    	String[] options = {"no View","Buy","Sell","Hold"}; 
        	    	Random random1 = new Random();
        	        int index = random1.nextInt(options.length); // generate a random index within the range of the array
        	        String randomOption = options[index];
        	    	response.put("Axis Expert View", randomOption);
        	    	randomNumber = random.nextInt(1000) + 1;
        	    	response.put("Viewed", randomNumber);
        	    	randomNumber = random.nextInt(1000) + 1;
        	    	response.put("Brought", randomNumber);
        	    	randomNumber = random.nextInt(1000) + 1;
        	    	response.put("WatchList", randomNumber);
        	    	String weblink ="https://omni.axisbank.co.in/axisretailbanking/";
        	    	response.put("Know More Button", weblink);
        	    	response.put("Invest More Button", weblink);
        	    	StringBuilder sb = new StringBuilder("{");
        	        for (Map.Entry<String, Object> entry : response.entrySet()) {
        	            String key = entry.getKey();
        	            Object value = entry.getValue();
        	            sb.append("\"").append(key).append("\":\"");
        	            if (value != null) {
        	                sb.append(value.toString());
        	            }
        	            sb.append("\",");
        	        }
        	        sb.deleteCharAt(sb.length() - 1); // remove the last comma
        	        sb.append("}");
        	        output.append(sb);
        	        output.append(",");
        		}
    		}catch(Exception ex) {
    			return "NA";
    		}
    		String response_string =output.toString();
    		String response_string_2=response_string.substring(0, response_string.length() - 1);
    		return "["+response_string_2+"]";
    	}
        return "NA";
    }
	
	@CrossOrigin
	@PostMapping("/get_customer_display_name")
	public String  getCustDisName(@RequestBody Map<String, Object> request) {
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			if(!request.get("Customer_Key").toString().equals("")) {
				System.out.println("Customer_Key Present");
			}
		}
		catch(Exception ex) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",121);
            err_response.put("desc","Null Customer_Key;Customer_Key Cannot be a null field; Please Add Valid Keys ");
            err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			System.out.println(Instant.now() + " | ERROR | " + "Customer_Key Cannot be a null field; Please Add Valid Keys ");	
            return "NA";
		}
		
		Object customer_key_obj = request.get("Customer_Key"); //Customer_Key
		Object reco_type_obj = request.get("Reco_Type"); //Customer_Key
		Object reco_count = request.get("Reco_Count"); //Customer_Key
		String customer_key=customer_key_obj.toString();
		Integer cust_key_valid_check = service.checkCustomerKey(customer_key);
		if(cust_key_valid_check==-1) {
			Map<String, Object> err_response = new HashMap<String, Object>();
			err_response.put("code",126);
		    err_response.put("desc","Given Customer_Key Does not Exist in DB; Please Enter a valid Customer_Key");
		    err_response.put("time",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		    return "NA";
		}
		String customer_first_name ="";
		customer_first_name= service.getFirstName(customer_key);
		response.put("Name", customer_first_name);
		
		StringBuilder sb = new StringBuilder("{");
        for (Map.Entry<String, Object> entry : response.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            sb.append("\"").append(key).append("\":\"");
            if (value != null) {
                sb.append(value.toString());
            }
            sb.append("\",");
        }
        sb.deleteCharAt(sb.length() - 1); // remove the last comma
        sb.append("}");
        return sb.toString();
	}
}
