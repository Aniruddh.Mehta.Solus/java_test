package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
public class RcoreStoryPoint {
	@Id
	@Column(name="Name")
	private String name;
	public   String getname(){ return  name ;}
	
}
