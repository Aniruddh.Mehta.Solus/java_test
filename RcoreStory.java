package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable

//@Table(name="RankedPickList_Stories_Master")
//@Access(value=AccessType.FIELD)
public class RcoreStory {
	@Column(name="Story_Id")
	private long storyId;
	
	@Id
	@Column(name="Name")
	private String name;
	
	@Column(name="recos")
	private String recos;
	
	@Column(name="Customer_Key")
	private String customerKey;
	
	public   long getstoryId(){ return  storyId ;}
	public   String getname(){ return  name ;}
	public   String getrecos(){ return  recos ;}
	public   String getcustomerKey(){ return  customerKey ;}
}
