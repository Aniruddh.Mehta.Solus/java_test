package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.Solus_Microservice.reco_API.entity.AnonRecoTypeCheck;

public interface AnonRecoTypeRepo extends JpaRepository<AnonRecoTypeCheck,String>{
	@Query(value="select Story_Id, Type from RankedPickList_Anonymous_Stories_Master where Type=:usecasekey ", nativeQuery=true)
	public List<AnonRecoTypeCheck> getAnonRecoTypeDetails(@Param("usecasekey") String reco_type);
}
