package com.solus.solus_url_shotener_service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.solus.solus_url_shotener_service.entity.RouteState;

@Repository
public interface RouteStateRepository  extends JpaRepository<RouteState,Long>{
	@Query(value="select ID ,Name,Value,Description,Modified_Date  from Microservices_Config where Name=:name ", nativeQuery=true)
	public List<RouteState> getStateName(@Param("name") String stateName); 
}
