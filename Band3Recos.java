package com.solus.solus_url_shotener_service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
public class Band3Recos {
	@Id
	@Column(name="Recos")
	private String recos;
	public  String getrecos(){ return  recos ;}
}
