package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.Solus_Microservice.reco_API.entity.ContextKeyProducts;

@Repository
public interface ContextKeyProductRepo  extends JpaRepository<ContextKeyProducts,String>{
	//@Query(value=" select Recos from RankedPickList_Anonymous_Stories where Object_Id=:ContextVal and Story_Id=:StoryId ", nativeQuery=true)
	@Query(value="select c.Product_Name as Recos ,  b.Product_Key as Product_Id ,  l4.LOV_Value ,  c.Custom_Tag1_LOV_Id as Bought,  c.Custom_Tag2_LOV_Id as Watchlist,  case c.Custom_Tag3_LOV_Id when 0 then 'NA' else l1.LOV_Value end as CMOTS_COCODE,  case c.Custom_Tag4_LOV_Id when 0 then 'NA' else l2.LOV_Value end as ISIN,   case c.Custom_Tag5_LOV_Id when 0 then 'NA' else l3.LOV_Value end as EXCHANGEE, case c.Custom_Tag6_LOV_Id when 0 then 'NA' else substring_index(l5.LOV_Value,':',-1) end as ODINCODE    from RankedPickList_Anonymous_Stories a left join CDM_Product_Key_Lookup b ON a.Recos=b.Product_Key  left join CDM_Product_Master c ON c.Product_Id=b.Product_Id  left join RankedPickList_Anonymous_Stories_Master d ON d.Story_Id=a.Story_Id  left join CDM_LOV_Master l1 ON l1.LOV_Id=c.Custom_Tag3_LOV_Id   left join CDM_LOV_Master l2 ON l2.LOV_Id=c.Custom_Tag4_LOV_Id   left join CDM_LOV_Master l3 ON l3.LOV_Id=c.Custom_Tag5_LOV_Id  left join CDM_LOV_Master l4 ON l4.LOV_Id=c.Cat_LOV_Id   left join CDM_LOV_Master l5 on l5.LOV_Id=c.Custom_Tag6_LOV_Id   where a.Story_Id=:StoryId  and a.Object_Id=:ContextVal  ", nativeQuery=true)
	public List<ContextKeyProducts>getContextProductList(@Param("ContextVal") String Context_Value,@Param("StoryId") Integer Story_Id);
}


//select Recos,Product_Id,LOV_Value from RankedPickList_Anonymous_Stories  a, CDM_Product_Master b ,CDM_LOV_Master c where Object_Id=:ContextVal and Story_Id=:StoryId  and  a.Recos=b.Product_Name  and  c.LOV_Id=b.Cat_LOV_Id 
//select c.Product_Name as Recos ,  b.Product_Key as Product_Id ,  l4.LOV_Value ,  c.Custom_Tag1_LOV_Id as Bought,  c.Custom_Tag2_LOV_Id as Watchlist,  case c.Custom_Tag3_LOV_Id when 0 then 'NA' else l1.LOV_Value end as CMOTS_COCODE,  case c.Custom_Tag4_LOV_Id when 0 then 'NA' else l2.LOV_Value end as ISIN,   case c.Custom_Tag5_LOV_Id when 0 then 'NA' else l3.LOV_Value end as EXCHANGEE   from RankedPickList_Anonymous_Stories a left join CDM_Product_Key_Lookup b ON a.Recos=b.Product_Key  left join CDM_Product_Master c ON c.Product_Id=b.Product_Id  left join RankedPickList_Anonymous_Stories_Master d ON d.Story_Id=a.Story_Id  left join CDM_LOV_Master l1 ON l1.LOV_Id=c.Custom_Tag3_LOV_Id   left join CDM_LOV_Master l2 ON l2.LOV_Id=c.Custom_Tag4_LOV_Id   left join CDM_LOV_Master l3 ON l3.LOV_Id=c.Custom_Tag5_LOV_Id  left join CDM_LOV_Master l4 ON l4.LOV_Id=c.Cat_LOV_Id  where a.Story_Id=:StoryId  and a.Object_Id=:ContextVal  
