package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.Solus_Microservice.reco_API.entity.CoPurchaseProducts;
@Repository
public interface CoPurchaseRecoList extends JpaRepository<CoPurchaseProducts,String>{
	@Query(value=" select Object_Id, Recos from RankedPickList_Anonymous_Stories where Object_Id=:ObjectId and Story_Id=:StoryId ", nativeQuery=true)
	public List<CoPurchaseProducts> getCoPurchaseProduct(@Param("ObjectId") String Object_Id,@Param("StoryId") Integer Story_Id);
}
