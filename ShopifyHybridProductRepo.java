package com.Solus_Microservice.reco_API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.Solus_Microservice.reco_API.entity.ShopifyHybridProductList;

@Repository
public interface ShopifyHybridProductRepo extends JpaRepository<ShopifyHybridProductList,String>{
	@Query(value="select d.Product_Key as Product_Variant_Id , a.Product_Id  ,SUBSTRING_INDEX(SUBSTRING_INDEX(Product_URL,'/',-1),'?',1) as Product_Handle   from RankedPickList a,CDM_Product_Master_Original b, CDM_Customer_Key_Lookup c,CDM_Product_Key_Lookup d where d.Product_Id=a.Product_Id and  a.Product_Id=b.Product_Id and a.Customer_Id=c.Customer_Id and Customer_Key=:cust_key ", nativeQuery=true)
	public List<ShopifyHybridProductList> getShopifyProductList(@Param("cust_key") String Customer_key);
}
