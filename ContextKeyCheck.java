package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
public class ContextKeyCheck {
	@Id
	@Column(name="Story_Id")
	private int storyId;
	public  int getStoryId(){ return  storyId ;}
}
