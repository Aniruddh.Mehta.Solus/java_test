package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;


@Entity
@Immutable
public class RecoTypeCheck {
	@Id
	@Column(name="UseCaseKey")
	private String usecaseKey;
	public  String getusecaseKey(){ return  usecaseKey ;}
	
	@Column(name="Story_Id")
	private int storyId;
	public  int getStoryId(){ return  storyId ;}
}