package com.Solus_Microservice.reco_API.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
public class LastMessageSent {
	@Id
//	@Column(name="Customer_Key")
//	private String cust_key;
//	public  String getcust_Key(){ return cust_key ;}
//	
	@Column(name="message")
	private String message;
	public  String getMessage(){ return message ;}
//	
//	@Column(name="Event_Execution_Date_Id")
//	private String EventDateId;
//	public  String getEventDateId(){ return EventDateId ;}
	
}
